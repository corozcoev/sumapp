  <div class="footer">
    <center> <img src="images/logo-union-letras.png" width="150" height="35"> </center>
            
            <p class="footer-text">La mejor experiencia a tu alcance</p>
          
            <p class="footer-copyright">Copyright &copy; Empresa Virtual <span id="copyright-year">2020</span>. Derechos reservados.</p>
        </div>   


<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/plugins.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>
<script type="text/javascript" src="scripts/charts.js"></script>
<script type="text/javascript" src="scripts/clock.js"></script>
<script src="scripts/binaryajax.js"></script>
<script src="scripts/exif.js"></script>
<script src="scripts/canvasResize.js"></script>
<script src="scripts/zepto.min.js"></script>
      <script>
      	var verticalChart = $('#inspeccionmes');
      	    var verticalDemoChart = new Chart(verticalChart, {
                        type: 'bar',
                        data: {
                          labels: [  
           <?php foreach($arrayinspeccionmes as $g1):?>
            "<?php echo $g1->MES?>",
            <?php endforeach; ?>],
                          datasets: [
                            {
                              label: "Total",
                              backgroundColor: "#A0D468",
                              data: [ <?php foreach($arrayinspeccionmes as $g1):?>
        <?php echo $g1->TOTAL;?>,
        <?php endforeach; ?>]
                            }
                          ]
                        },
                        options: {
                            responsive: true, maintainAspectRatio:false,
                            legend: {display: true, position:'bottom', labels:{fontSize:13, padding:15,boxWidth:12},},
                            title: {display: false}
                        }
                    });	
</script>
<script>
var ctx = document.getElementById("Noconformidades");
var data = {
        labels: [
        <?php foreach($arraynoconformidad as $g2):?>
        "<?php echo $g2->MES?>",
        <?php endforeach; ?>
        ],
        datasets: [{
            label: 'Total no conformidades',
            data: [
        <?php foreach($arraynoconformidad as $g2):?>
        <?php echo $g2->TOTAL;?>,
        <?php endforeach; ?>
            ],
            backgroundColor: "#0CC27E",
            borderColor: "#0CC27E",
            borderWidth: 2
        }]
    };
var options = { 
        scales: {
           yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'No conformidades'
          }
        }],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Meses'
          }
        }],
        }
    };
var chart1 = new Chart(ctx, {
    type: 'bar', /* valores: line, bar*/
    data: data,
    options: options
});
</script>
<script>
var ctx = document.getElementById("Noconformidadescamarista");
var data = {
        labels: [
        <?php foreach($arraynoconformidadcamarista as $g2):?>
        "<?php echo substr($g2->camarista,0,6)?>",
        <?php endforeach; ?>
        ],
        datasets: [{
            label: 'Total no conformidades',
            data: [
        <?php foreach($arraynoconformidadcamarista as $g2):?>
        <?php echo $g2->TOTAL;?>,
        <?php endforeach; ?>
            ],
            backgroundColor: "#1CBCD8",
            borderColor: "#1CBCD8",
            borderWidth: 2
        }]
    };
var options = {
tooltips: {

        callbacks: {
            title : (tooltipItems, data) => {
                var punto = '';
               var labelIndex = tooltipItems[0].index;
               var realLabel = data.labels[labelIndex];
               <?php foreach($arraynoconformidadcamarista as $g2):?>
               if (realLabel=='<?php echo substr($g2->camarista,0,6)?>') {
               punto ='<?php echo $g2->camarista?>';
               }
               <?php endforeach; ?>
               return realLabel +"-"+ punto;
      }
    }
  }, 
        scales: {
           yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'No conformidades'
          }
        }],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Camaristas'
          }
        }],
        }
    };
var chart1 = new Chart(ctx, {
    type: 'bar', /* valores: line, bar*/
    data: data,
    options: options
});
</script>  
<script>
var ctx = document.getElementById("Inspeccionsupervisor");
var data = {
        labels: [
        <?php foreach($arrayinspeccionsupervisor as $g2):?>
        "<?php echo substr($g2->email,0,8)?>",
        <?php endforeach; ?>
        ],
        datasets: [{
            label: 'Total no conformidades',
            data: [
        <?php foreach($arrayinspeccionsupervisor as $g2):?>
        <?php echo $g2->TOTAL;?>,
        <?php endforeach; ?>
            ],
            backgroundColor: "#0CC27E",
            borderColor: "#0CC27E",
            borderWidth: 2
        }]
    };
var options = {
tooltips: {

        callbacks: {
            title : (tooltipItems, data) => {
                var punto = '';
               var labelIndex = tooltipItems[0].index;
               var realLabel = data.labels[labelIndex];
               <?php foreach($arrayinspeccionsupervisor as $g2):?>
               if (realLabel=='<?php echo substr($g2->email,0,8)?>') {
               punto ='<?php echo $g2->email?>';
               }
               <?php endforeach; ?>
               return  punto;
      }
    }
  }, 
        scales: {
           yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'No conformidades'
          }
        }],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Supervisores'
          }
        }],
        }
    };
var chart1 = new Chart(ctx, {
    type: 'bar', /* valores: line, bar*/
    data: data,
    options: options
});
</script>
<script>
var ctx = document.getElementById("incidenciasporpregunta");
var data = {
        labels: [ 
        <?php foreach($arraypnoconformidad as $g3):?>
        "<?php echo $g3->PREGUNTA?>", 
        <?php endforeach; ?>
        ],
        datasets: [{
            label: 'Total de incidencias por pregunta',
            data: [
        <?php foreach($arraypnoconformidad as $g3):?>
        <?php echo $g3->TOTAL;?>, 
        <?php endforeach; ?>
            ],
            backgroundColor: "#FF586B",
            borderColor: "#FF586B",
            borderWidth: 2
        }]
    };
var options = {
   tooltips: {

        callbacks: {
            title : (tooltipItems, data) => {
                var punto = '';
               var labelIndex = tooltipItems[0].index;
               var realLabel = data.labels[labelIndex];
               if (realLabel=='P1') {
               punto ='Puerta limpia, mirilla, ruta de escape.';
               }
                if (realLabel=='P2') {
               punto ='Paredes y techo limpios y sin rayas. Rejillas  e interruptores limpios.';
               }
                 if (realLabel=='P3') {
               punto ='Piso  limpios. Verificar limpieza  abajo de la cama';
               }
                 if (realLabel=='P4') {
               punto ='Espejo limpio';
               }
                 if (realLabel=='P5') {
               punto ='Televisor,muebles (banca, silla, escritorio, armario c/ 5 colgadores + 1 c/ 2 presillas) sin polvo o suciedad.';
               }
                 if (realLabel=='P6') {
               punto ='Funcionamiento televisor, control remoto, termostato (clima), telefono.';
               }
                 if (realLabel=='P7') {
               punto ='Almohada extra (con funda) y cobertor extra envueltos.';
               }
                 if (realLabel=='P8') {
               punto ='Cortinas limpias, ningun gancho suelto, sin hoyos o descosidos, limpieza de ventana';
               }
                 if (realLabel=='P9') {
               punto ='Iluminación sin polvo y funcionando.';
               }
                 if (realLabel=='P10') {
               punto ='Cama: blancos limpios y bien tendidos (duvet/cubre cama).';
               }
                 if (realLabel=='P11') {
               punto ='No Molestar", bolsa y  lista de lavandería, hotel info, almohadita, Contrato 15 minutos, Menu snacks.';
               }
                 if (realLabel=='P12') {
               punto ='Indicación de voltage (habitación y baño)';
               }
                 if (realLabel=='P13') {
               punto ='2 dispensadores, 2 vasos estuchados, bolsa basura, 2 rollos papel higiénico y candado de ahorro de toalla.';
               }
                 if (realLabel=='P14') {
               punto ='Lavabo y grifos limpio y seco. Presencia tapa WC y funcionamiento secadora, portapapel, toallero.';
               }
                 if (realLabel=='P15') {
               punto ='Espejo, iluminación, pared, piso y rejilla de extracción limpios. Plafones limpios.';
               }
                 if (realLabel=='P16') {
               punto ='2 toallas de baño, 1 Toalla tapete sin manchas u hoyos';
               }
                 if (realLabel=='P17') {
               punto ='WC limpio y seco sin fugas. Regadera sin fuga.';
               }
                 if (realLabel=='P18') {
               punto ='Puerta de cristal, azulejos, jabonera y alcantarilla limpios y secos.';
               }
                 if (realLabel=='P19') {
               punto ='Rejunte limpio y sin moho.';
               }
               return realLabel +"-"+ punto;
      }
    }
  },
        scales: {
           yAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'No conformidades por pregunta'
          }
        }],
        xAxes: [{
          scaleLabel: {
            display: true,
            labelString: 'Preguntas'
          }
        }],
        }
    };
var chart1 = new Chart(ctx, {
    type: 'bar', /* valores: line, bar*/
    data: data,
    options: options
});
</script>