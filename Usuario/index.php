

<!DOCTYPE HTML>
<html lang="en">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1, viewport-fit=cover" />
<meta name="apple-mobile-web-app-status-bar-style" content="black-translucent">
<meta name="apple-mobile-web-app-capable" content="yes">
<title>SuMapp </title>
<!-- Don't forget to update PWA version (must be same) in pwa.js & manifest.json -->
<link rel="manifest" href="manifest.json" data-pwa-version="1.0">

<link href="https://fonts.googleapis.com/css?family=Poppins:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i,900,900i" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="styles/style.css">
<link rel="stylesheet" type="text/css" href="styles/framework.css">
<link rel="stylesheet" type="text/css" href="fonts/css/fontawesome-all.min.css">    
<link rel="apple-touch-icon" sizes="57x57" href="images/apple-icon-57x57.png">
<link rel="apple-touch-icon" sizes="60x60" href="images/apple-icon-60x60.png">
<link rel="apple-touch-icon" sizes="72x72" href="images/apple-icon-72x72.png">
<link rel="apple-touch-icon" sizes="76x76" href="images/apple-icon-76x76.png">
<link rel="apple-touch-icon" sizes="114x114" href="images/apple-icon-114x114.png">
<link rel="apple-touch-icon" sizes="120x120" href="images/apple-icon-120x120.png">
<link rel="apple-touch-icon" sizes="144x144" href="images/apple-icon-144x144.png">
<link rel="apple-touch-icon" sizes="152x152" href="images/apple-icon-152x152.png">
<link rel="apple-touch-icon" sizes="180x180" href="images/apple-icon-180x180.png">
<link rel="icon" type="image/png" sizes="192x192"  href="images/android-icon-192x192.png">
<link rel="icon" type="image/png" sizes="32x32" href="images/favicon-32x32.png">
<link rel="icon" type="image/png" sizes="96x96" href="images/favicon-96x96.png">
<link rel="icon" type="image/png" sizes="16x16" href="images/favicon-16x16.png">
<meta name="msapplication-TileColor" content="#ffffff">
<meta name="msapplication-TileImage" content="images/ms-icon-144x144.png">
<meta name="theme-color" content="#ffffff"> 
   <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.css"/>

   <script src="https://cdnjs.cloudflare.com/ajax/libs/limonte-sweetalert2/6.11.0/sweetalert2.js"></script>

</head> 
    <?php
session_start();
include('includes/config.php');
if(isset($_POST['login']))
{
$status='1';
$email=$_POST['username'];
$password=$_POST['password'];
$sql ="SELECT correo,contrasena FROM tb_usuario WHERE correo=:email and contrasena=:password and estado=(:status)";
$query= $dbh -> prepare($sql);
$query-> bindParam(':email', $email, PDO::PARAM_STR);
$query-> bindParam(':password', $password, PDO::PARAM_STR);
$query-> bindParam(':status', $status, PDO::PARAM_STR);
$query-> execute();
$results=$query->fetchAll(PDO::FETCH_OBJ);
if($query->rowCount() > 0)
{
$_SESSION['alogin']=$_POST['username'];
echo "<script type='text/javascript'> document.location = 'tablero.php'; </script>";
} else{
  
  echo '<script>

 swal({
   title: "¡ERROR!",
   text: "Datos de acceso incorrectos",
   type: "error",
 });

                     
</script>';

}

}

?>
<body class="theme-light" data-highlight="blue2">
    
<div id="page-preloader">
    <div class="loader-main"><div class="preload-spinner border-highlight"></div></div>
</div>
    
    
<div id="page">        
	
    
    <div class="page-content-white"></div>    
    
	<div class="page-content">	
        <div class="cover-wrapper cover-no-buttons">
            <div data-height="cover" class="caption bottom-0">
                <div class="caption-center">

                    <div class="left-50 right-50">
                        <center><img src="images/logo-union-letras.png" width="200px" height="70px"></center>
                        
                        <br>
                        <br>
                        <div class="input-style input-light has-icon input-style-1 input-required">
                        	<form method="post">
                            <i class="input-icon fa fa-user font-11"></i>
                            <span>Usuario</span>
                            <em>(requerido)</em>
                            <input type="name" name="username" placeholder="Ingrese su usuario">
                        </div> 
                        <div class="input-style input-light has-icon input-style-1 input-required bottom-30">
                            <i class="input-icon fa fa-lock font-11"></i>
                            <span>Contraseña</span>
                            <em>(requerido)</em>
                            <input type="password" name="password" placeholder="Ingrese su contraseña">
                        </div>          
                        <!--<div class="one-half">
                            <a href="pageapp-register.html" class="font-11 color-white opacity-50">Crear cuenta</a>
                        </div>-->
                        <!--<div class="one-half last-column">
                            <a href="pageapp-forgot.html" class="text-right font-11 color-white opacity-50">¿Olvide contraseña?</a>
                        </div>-->
                        <div class="clear"></div>
                        <center>
                     <button class=" button button-full button-m shadow-large button-round-small bg-highlight top-30 bottom-0" name="login" type="submit">Ingresar&nbsp;<span class="fa-fw select-all fas"></span></button>
                     </center>
                    </div>
</form>
<br>    
<center><a class=" button button-xs shadow-small button-round-small bg-teal-dark" href="mailto:soporte@sumapp.cloud?Subject=Tengo un problema">Soporte&nbsp;<span class="fa-fw select-all fas"></span></a></center>

                </div>
            </div>   
            <div class="caption-overlay "></div>
            <div class="caption-bg" style="background-image:url(images/login.jpg)"></div>
        </div>
    </div>
        

    
    <div class="menu-hider"></div>
</div>
    
<div id="menu-install-pwa-android" class="menu menu-box-modal round-medium" data-menu-height="340" 
    data-menu-width="320" 
    data-menu-effect="menu-over">
    <div class="boxed-text-huge top-25">
        <img class="round-medium center-horizontal" src="images/apple-icon-120x120.png" alt="img" width="50">
        <h4 class="center-text bolder top-20 bottom-10"><span class="color-highlight">Sumapp</span> Agregar app a pantalla principal</h4>
        <p>
           Instale Sumapp en su pantalla de inicio y acceda a ella como una aplicación normal.
        </p>
        <a href="#" class="pwa-install button button-xs button-round-medium button-center-large shadow-large bg-highlight bottom-0">Agregar a inicio</a><br>
        <a href="#" class="pwa-dismiss close-menu center-text color-gray2-light uppercase ultrabold opacity-80 under-heading">Mas tarde</a>
        <div class="clear"></div>
    </div>
</div>   

<!-- Install instructions for iOS -->
<div id="menu-install-pwa-ios" 
    class="menu menu-box-bottom round-medium bottom-20 left-20 right-20" 
    data-menu-height="320" 
    data-menu-width="320" 
    data-menu-effect="menu-over">
    <div class="boxed-text-huge top-25">
        <img class="round-medium center-horizontal" src="images/apple-icon-120x120.png" alt="img" width="50">
        <h4 class="center-text bolder top-20 bottom-10"><span class="color-highlight">Sumapp</span> Agregar app a pantalla principal</h4>
        <p class="bottom-15">
            <center> <img src="images/pasos-ios.png" width="250" height="110"></center>
         
        </p>

        <div class="clear"></div>
        <a href="#" class="pwa-dismiss close-menu center-text color-red2-dark uppercase ultrabold opacity-80 top-25">Mas tarde</a>

        <i class="fa-ios-arrow fa fa-caret-down font-40"></i>
    </div>
</div>


<script type="text/javascript" src="scripts/jquery.js"></script>
<script type="text/javascript" src="scripts/plugins.js"></script>
<script type="text/javascript" src="scripts/custom.js"></script>

</body>


