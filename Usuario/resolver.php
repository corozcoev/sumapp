
<?php include 'includes/header.php'; ?>
     <?php
     $idbloque=$_GET['bloque'];
     $cuestionario=$_GET['cuestionario'];
     $sucursal=$_GET['sucursal'];
     $sqlbloque="SELECT c_nombre_bloque,respuesta_predeterminada FROM tb_encuesta_bloque WHERE id_bloque='".$idbloque."'";
$querybloque = $conexion->query($sqlbloque);//Se ejecuta consulta
$arraybloque= array(); // Array donde vamos a guardar los datos 
while($resultadobloque = $querybloque->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraybloque[]=$resultadobloque; // Guardar los resultados en la variable

}
 $sqlasignacionps="SELECT COUNT(id_bloque) AS asignaciones FROM tb_asignacion_ps 
WHERE id_bloque='".$idbloque."'";
$queryasignacionps = $conexion->query($sqlasignacionps);//Se ejecuta consulta
$arrayasignacionps= array(); // Array donde vamos a guardar los datos 
while($resultadoasignacionps = $queryasignacionps->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayasignacionps[]=$resultadoasignacionps; // Guardar los resultados en la variable

}

foreach ($arraybloque as $rp) {
$respre=$rp->respuesta_predeterminada;
}

if ($respre==1) {
  $respsi="checked";
  $respno="";
}
if ($respre==2) {
   $respsi="";
  $respno="checked";
}
if ($respre=='ninguno') {
 $respsi="";
  $respno="";
}
      ?>
<style >
  .file-upload{
  height:100px;
  width:100px;
  margin:40px auto;
  border:1px solid #0B98CB;
  background: white;
  border-radius:100px;
  overflow:hidden;
  position:relative;
}
.file-upload input{
  position:absolute;
  height:400px;
  width:400px;
  left:-200px;
  top:-200px;
  background:transparent;
  opacity:0;
  -ms-filter:"progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: alpha(opacity=0);  
}
.file-upload img{
  height:70px;
  width:70px;
  margin:15px;
}
.hiddenFileInput > input{
  height: 0;
  width: 0;
  opacity: 0;
  cursor: pointer;
}
.hiddenFileInput{

  width: 0px;
  height: 0px;
  display: inline-block;
  overflow: hidden;
  cursor: pointer;
  
}
.lds-dual-ring.hidden { 
display: none;
}
.lds-dual-ring {
  display: inline-block;
  width: 80px;
  height: 80px;
}
.lds-dual-ring:after {
  content: " ";
  display: block;
  width: 64px;
  height: 64px;
  margin: 5% auto;
  border-radius: 50%;
  border: 6px solid #fff;
  border-color: #fff transparent #fff transparent;
  animation: lds-dual-ring 1.2s linear infinite;
}
@keyframes lds-dual-ring {
  0% {
    transform: rotate(0deg);
  }
  100% {
    transform: rotate(360deg);
  }
}


.overlay {
    position: fixed;
    top: 0;
    left: 0;
    width: 100%;
    height: 100vh;
    background: rgba(0,0,0,.8);
    z-index: 999;
    opacity: 1;
    transition: all 0.5s;
}
</style>   
<div id="page"> 
<div id="loader" class="lds-dual-ring hidden overlay">
  <br>
  <br>
  <br>
  <center> <h2 style="color: white">Revisando irregularidades y subiendo cuestionario</h2>
    <br>
    <h4 style="color: white">Porfavor espere</h4></center>
 
</div>  
<div class="header header-fixed header-logo-app">
    <?php foreach ($arraybloque as $bloque): ?>
        <a href="#" class="header-title"><?php echo $bloque->c_nombre_bloque ?></a>
    <?php endforeach; ?>
    <a href="#" class="header-icon header-icon-1" data-back-button><i class="fas fa-arrow-left"></i></a>
        <a href="#" class="header-icon header-icon-2" data-menu="menu-1"><i class="fas fa-bars"></i></a>
        <a href="mailto:soporte@sumapp.cloud?Subject=Tengo un problema" class="header-icon header-icon-3"><i class="fa fa-envelope"></i></a>
        <a href="#" class="header-icon header-icon-4" data-toggle-theme><i class="fas fa-moon"></i></a>
	</div>
	<?php include('includes/menu.php');?>
    	 <?php
	 $sqlpregunta="SELECT id_pregunta,c_titulo_pregunta,c_tipo_pregunta,clave_pregunta,n_orden_pregunta FROM tb_encuesta_pregunta WHERE id_bloque='".$idbloque."' ORDER BY n_orden_pregunta ASC ";
$querypregunta = $conexion->query($sqlpregunta);//Se ejecuta consulta
$arraypregunta= array(); // Array donde vamos a guardar los datos 
while($resultadopregunta = $querypregunta->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraypregunta[]=$resultadopregunta; // Guardar los resultados en la variable

}
   $sqltotallista="SELECT COUNT(id_pregunta) AS Total FROM tb_encuesta_pregunta WHERE id_bloque='".$idbloque."' AND c_tipo_pregunta='Lista' ";
$querytotallista = $conexion->query($sqltotallista);//Se ejecuta consulta
$arraytotallista= array(); // Array donde vamos a guardar los datos 
while($resultadototallista = $querytotallista->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraytotallista[]=$resultadototallista; // Guardar los resultados en la variable

}

foreach ($arraytotallista as $total) {
  $totallista=$total->Total;
}
   if ($totallista>0) {
   
 $sqlpreguntalista="SELECT id_pregunta FROM tb_encuesta_pregunta WHERE c_tipo_pregunta='Lista' AND id_bloque='".$idbloque."'  ORDER BY n_orden_pregunta ASC LIMIT 1 ";
$querypreguntalista = $conexion->query($sqlpreguntalista);//Se ejecuta consulta
$arraypreguntalista= array(); // Array donde vamos a guardar los datos 
while($resultadopreguntalista = $querypreguntalista->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraypreguntalista[]=$resultadopreguntalista; // Guardar los resultados en la variable

}

   foreach ($arraypreguntalista as $preguntalista):
     $sqllista="SELECT A.c_nombre_atributo FROM tb_encuesta_pregunta_lista PL 
INNER JOIN tb_encuesta_pregunta P ON PL.id_pregunta=P.id_pregunta
INNER JOIN tb_lista L ON L.id_lista=PL.id_lista
INNER JOIN tb_atributo A ON A.id_lista=L.id_lista
WHERE PL.id_pregunta='".$preguntalista->id_pregunta."' ";
endforeach;

$querylista = $conexion->query($sqllista);//Se ejecuta consulta
$arraylista= array(); // Array donde vamos a guardar los datos 
while($resultadolista = $querylista->fetch_assoc()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $atributos.='<option>'.$resultadolista['c_nombre_atributo'].'</option>'; // Guardar los resultados en la variable

}
}
      ?>
 
         
    <div class="page-content header-clear-medium">	    
    <div class="content">
       <script type="text/javascript">
           function actualizar(){location.href='cuestionarios.php';}
            $( document ).ready(function() 
{
   
    
$("form#data").submit(function(event)
{

    $(".capa").fadeIn(200);
    event.preventDefault();

    var formData = new FormData($(this)[0]);

    console.log(formData)

  $.ajax({
    url: 'acciones/guardar.php',
    type: 'POST',
    data: formData,

  
    contentType: false,
    processData: false,
        beforeSend: function() {
        $('#loader').removeClass('hidden')
    },
    success: function (returndata)
    {
        if(returndata == "2")
        { 
            alert("Error al guardar")
        } 
        else 
        {
            Swal.fire({
  title: '<strong>ENVIO EXITOSO</strong>',
   text: 'empresavirtual.mx',
  icon: 'success',
    confirmButtonText:
    '<i class="fa fa-thumbs-up"></i> Aceptar'
})
    setInterval("actualizar()",2000); 
        }
        $(".capa").fadeOut(200);
    },
      complete: function(){
        $('#loader').addClass('hidden')
    },
  });
 
  return false;
  });
  
});


        </script>
        <form id="data">
           <?php foreach ($arraybloque as $bloque): ?>
         <center><h3 class="bolder"><?php echo $bloque->c_nombre_bloque ?></h3>
         <?php endforeach; ?>
            <p>
              Porfavor revise su informacion antes de guardar.
            </p>

            <?php
$piso=$_GET['piso'];
$sala=$_GET['sala']; 

     $sqlps="SELECT SU.sucursal,P.piso,S.sala FROM tb_asignacion_ps PS
INNER JOIN tb_piso P ON P.id_piso=PS.id_piso
INNER JOIN tb_sala S ON S.id_sala=PS.id_sala
INNER JOIN tb_sucursal SU ON SU.id_sucursal=PS.id_sucursal
WHERE PS.id_piso='".$piso."' AND PS.id_sala='".$sala."' AND PS.id_sucursal='".$sucursal."' ";


$queryps = $conexion->query($sqlps);//Se ejecuta consulta
$arrayps= array(); // Array donde vamos a guardar los datos 
while($resultadops = $queryps->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayps[]=$resultadops; // Guardar los resultados en la variable

}
foreach ($arrayasignacionps as $aps) {
$asignaciones=$aps->asignaciones;	
}
if ($asignaciones>0) {
if (($piso && $sala)!='') { 
foreach ($arrayps as $ps): 	?>
    <div class="divider divider-margins"></div>
               <h5>Sucursal</h5> <div class="input-style input-style-1 input-required">
                <em>(requerido)</em>
                <input type="text" name="sucursal" value="<?php echo $ps->sucursal ?>" disabled>
            </div>
<div class="divider divider-margins"></div>
               <h5>Piso</h5> <div class="input-style input-style-1 input-required">
                <em>(requerido)</em>
                <input type="text" name="piso" value="<?php echo $ps->piso ?>" disabled>
            </div>
<div class="divider divider-margins"></div>
               <h5>Sala</h5> <div class="input-style input-style-1 input-required">
                <em>(requerido)</em>
                <input type="text" name="sala" value="<?php echo $ps->sala ?>" disabled>
            </div>
<?php 
endforeach;
}}
?>
<input type="hidden" name="usuario" value="<?php echo $_SESSION['alogin'] ?>">
<input type="hidden" name="cuestionario" value="<?php echo $cuestionario ?>">
<input type="hidden" name="bloque" value="<?php echo $idbloque ?>">
<input type="hidden" name="sucursal" value="<?php echo $sucursal ?>">

<script type="text/javascript">
  // llama la función miUbicacion cuando la página este cargada
window.onload = miUbicacion;

function miUbicacion(){
    //Si los servicios de geolocalización están disponibles
    if(navigator.geolocation){
    // Para obtener la ubicación actual llama getCurrentPosition.
    navigator.geolocation.getCurrentPosition( muestraMiUbicacion );
    }else{ //de lo contrario
    alert("Los servicios de geolocalizaci\363n  no est\341n disponibles");
    }
}
function muestraMiUbicacion(posicion){
    var latitud = posicion.coords.latitude
    var longitud = posicion.coords.longitude

    var rlongitud = document.getElementById("longitud");
    rlongitud.value = longitud;
    var rlatitud = document.getElementById("latitud");
    rlatitud.value = latitud;
}
</script>

<input type="hidden" name="longitud" value="" id="longitud">
<input type="hidden" name="latitud" value="" id="latitud">

            <?php foreach ($arraypregunta as $pregunta): ?>
                <?php
                if ($pregunta->c_tipo_pregunta=='Lista') {
                echo '<div class="divider divider-margins"></div>
                <input type="hidden" name="pregunta[]" value="'.$pregunta->id_pregunta.'">
                            <h5>'.$pregunta->c_titulo_pregunta.'</h5><div class="input-style input-style-2 input-required">
                <span>Opciones</span>
                <em><i class="fa fa-angle-down"></i></em>
                <select name="respuesta[]">
                    <option value="default" disabled selected>Selecciona una opcion</option>
                          '.$atributos.'</select></div>
                           <input type="file" name="evidencia[]"  class="hiddenFileInput">
                            <input type="hidden" value="" name="comentario[]"  >'; 

                }if ($pregunta->c_tipo_pregunta=='RadioButton') {
                  echo ' <div class="divider divider-margins"></div>
                                 <h5>'.$pregunta->c_titulo_pregunta.'</h5>
                                 <input type="hidden" name="pregunta[]" value="'.$pregunta->id_pregunta.'">
                             <div class="fac fac-checkbox fac-blue"><span></span>
            <input id="'.$pregunta->c_titulo_pregunta.'si" name="respuesta[]" onclick="mostrar'.$pregunta->clave_pregunta.'(this.value);" type="checkbox" value="Si" '.$respsi.'>
            <label for="'.$pregunta->c_titulo_pregunta.'si">Si</label>
          </div>
  <div class="fac fac-checkbox fac-red"><span></span>
            <input id="'.$pregunta->c_titulo_pregunta.'no" name="respuesta[]" onclick="mostrar'.$pregunta->clave_pregunta.'(this.value);" type="checkbox" value="No" '.$respno.' >
            <label for="'.$pregunta->c_titulo_pregunta.'no">No</label>
          </div>

<script type="text/javascript">

function mostrar'.$pregunta->clave_pregunta.'(val) {
  console.log(val);
    if (val=="No") {
       
        document.getElementById("'.$pregunta->c_titulo_pregunta.'si").checked= false;
    }
 if (val=="Si") {
   
        document.getElementById("'.$pregunta->c_titulo_pregunta.'no").checked= false;
    }
 }

</script>
';
                }if ($pregunta->c_tipo_pregunta=='RadioButton-Ev') {
                  echo '
                                   
                  <div class="divider divider-margins"></div>
                                 <h5>'.$pregunta->c_titulo_pregunta.'</h5>
                                 <input type="hidden" name="pregunta[]" value="'.$pregunta->id_pregunta.'">
                             <div class="fac fac-checkbox fac-blue"><span></span>
            <input id="'.$pregunta->c_titulo_pregunta.'si" name="respuesta[]" onclick="mostrar'.$pregunta->clave_pregunta.'(this.value);" type="checkbox" value="Si" '.$respsi.'>
            <label for="'.$pregunta->c_titulo_pregunta.'si">Si</label>
          </div>
  <div class="fac fac-checkbox fac-red"><span></span>
            <input id="'.$pregunta->c_titulo_pregunta.'no" name="respuesta[]" onclick="mostrar'.$pregunta->clave_pregunta.'(this.value);" type="checkbox" value="No" '.$respno.' >
            <label for="'.$pregunta->c_titulo_pregunta.'no">No</label>
          </div>
                         <div class="flexbox-container" id="'.$pregunta->c_titulo_pregunta.'" style="display:none;">

  <div class="file-upload">
<!--place upload image/icon first !-->
<img id="i'.$pregunta->clave_pregunta.'" src="images/evidencia.png" />
<!--place input file last !-->
<input type="file" name="evidencia[]" id="'.$pregunta->clave_pregunta.'" accept="image/*" capture="camera" />
</div>
                                        
</div>
<script type="text/javascript">

function mostrar'.$pregunta->clave_pregunta.'(val) {
  console.log(val);
    if (val=="No") {
        document.getElementById("'.$pregunta->c_titulo_pregunta.'").style.display = "block";
        document.getElementById("'.$pregunta->c_titulo_pregunta.'si").checked= false;
    }
 if (val=="Si") {
        document.getElementById("'.$pregunta->c_titulo_pregunta.'").style.display = "none";
        document.getElementById("'.$pregunta->c_titulo_pregunta.'no").checked= false;
    }
 }

</script>
<script type="text/javascript">
// Used for creating a new FileList in a round-about way
function FileListItem(a) {
  a = [].slice.call(Array.isArray(a) ? a : arguments)
  for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
  if (!d) throw new TypeError("expected argument to FileList is File or array of File objects")
  for (b = (new ClipboardEvent("")).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
  return b.files
}

'.$pregunta->clave_pregunta.'.onchange = function change() {
  const file = this.files[0]
    if (!file) return
  
    file.image().then(img => {
    const canvas = document.createElement("canvas")
        const ctx = canvas.getContext("2d")
    const maxWidth = 250
    const maxHeight = 250
    
    // calculate new size
    const ratio = Math.min(maxWidth / img.width, maxHeight / img.height)
    const width = img.width * ratio + .5|0
    const height = img.height * ratio + .5|0
    
    // resize the canvas to the new dimensions
    canvas.width = width
    canvas.height = height
    
    // scale & draw the image onto the canvas
    ctx.drawImage(img, 0, 0, width, height)
    
    
    // Get the binary (aka blob)
    canvas.toBlob(blob => {
      const resizedFile = new File([blob], file.name, file)
      const fileList = new FileListItem(resizedFile)
      
      // temporary remove event listener since
      // assigning a new filelist to the input
      // will trigger a new change event...
      '.$pregunta->clave_pregunta.'.onchange = null
      '.$pregunta->clave_pregunta.'.files = fileList
      '.$pregunta->clave_pregunta.'.onchange = change
    })
  })
}

 $(function() {
   const i'.$pregunta->clave_pregunta.' = document.querySelector("#i'.$pregunta->clave_pregunta.'");
  const '.$pregunta->clave_pregunta.' = document.querySelector("#'.$pregunta->clave_pregunta.'");
  '.$pregunta->clave_pregunta.'.addEventListener("change", function(e) {
    i'.$pregunta->clave_pregunta.'.src = URL.createObjectURL(e.target.files[0]);
  });

});
</script>
                  ';
                }

if ($pregunta->c_tipo_pregunta=='RadioButton-Ev-Com') {
                  echo '
                                   
                  <div class="divider divider-margins"></div>
                                 <h5>'.$pregunta->c_titulo_pregunta.'</h5>
                                 <input type="hidden" name="pregunta[]" value="'.$pregunta->id_pregunta.'">
                             <div class="fac fac-checkbox fac-blue"><span></span>
            <input id="'.$pregunta->c_titulo_pregunta.'si" name="respuesta[]" onclick="mostrar'.$pregunta->clave_pregunta.'(this.value);" type="checkbox" value="Si" '.$respsi.'>
            <label for="'.$pregunta->c_titulo_pregunta.'si">Si</label>
          </div>
  <div class="fac fac-checkbox fac-red"><span></span>
            <input id="'.$pregunta->c_titulo_pregunta.'no" name="respuesta[]" onclick="mostrar'.$pregunta->clave_pregunta.'(this.value);" type="checkbox" value="No" '.$respno.' >
            <label for="'.$pregunta->c_titulo_pregunta.'no">No</label>
          </div>
                         <div class="flexbox-container" id="'.$pregunta->c_titulo_pregunta.'" style="display:none;">

  <div class="file-upload">
<!--place upload image/icon first !-->
<img id="i'.$pregunta->clave_pregunta.'" src="images/evidencia.png" />
<!--place input file last !-->
<input type="file" name="evidencia[]" id="'.$pregunta->clave_pregunta.'" accept="image/*" capture="camera" />

</div>
   <input type="text" name="comentario[]" placeholder="Ingrese observacion">                                     
</div>
<script type="text/javascript">

function mostrar'.$pregunta->clave_pregunta.'(val) {
  console.log(val);
    if (val=="No") {
        document.getElementById("'.$pregunta->c_titulo_pregunta.'").style.display = "block";
        document.getElementById("'.$pregunta->c_titulo_pregunta.'si").checked= false;
    }
 if (val=="Si") {
        document.getElementById("'.$pregunta->c_titulo_pregunta.'").style.display = "none";
        document.getElementById("'.$pregunta->c_titulo_pregunta.'no").checked= false;
    }
 }

</script>
<script type="text/javascript">
// Used for creating a new FileList in a round-about way
function FileListItem(a) {
  a = [].slice.call(Array.isArray(a) ? a : arguments)
  for (var c, b = c = a.length, d = !0; b-- && d;) d = a[b] instanceof File
  if (!d) throw new TypeError("expected argument to FileList is File or array of File objects")
  for (b = (new ClipboardEvent("")).clipboardData || new DataTransfer; c--;) b.items.add(a[c])
  return b.files
}

'.$pregunta->clave_pregunta.'.onchange = function change() {
  const file = this.files[0]
    if (!file) return
  
    file.image().then(img => {
    const canvas = document.createElement("canvas")
        const ctx = canvas.getContext("2d")
    const maxWidth = 250
    const maxHeight = 250
    
    // calculate new size
    const ratio = Math.min(maxWidth / img.width, maxHeight / img.height)
    const width = img.width * ratio + .5|0
    const height = img.height * ratio + .5|0
    
    // resize the canvas to the new dimensions
    canvas.width = width
    canvas.height = height
    
    // scale & draw the image onto the canvas
    ctx.drawImage(img, 0, 0, width, height)
    
    
    // Get the binary (aka blob)
    canvas.toBlob(blob => {
      const resizedFile = new File([blob], file.name, file)
      const fileList = new FileListItem(resizedFile)
      
      // temporary remove event listener since
      // assigning a new filelist to the input
      // will trigger a new change event...
      '.$pregunta->clave_pregunta.'.onchange = null
      '.$pregunta->clave_pregunta.'.files = fileList
      '.$pregunta->clave_pregunta.'.onchange = change
    })
  })
}

 $(function() {
   const i'.$pregunta->clave_pregunta.' = document.querySelector("#i'.$pregunta->clave_pregunta.'");
  const '.$pregunta->clave_pregunta.' = document.querySelector("#'.$pregunta->clave_pregunta.'");
  '.$pregunta->clave_pregunta.'.addEventListener("change", function(e) {
    i'.$pregunta->clave_pregunta.'.src = URL.createObjectURL(e.target.files[0]);
  });

});
</script>

<center>

</center>
                  ';
                }
                if ($pregunta->c_tipo_pregunta=='Texto') {
                  echo'<div class="divider divider-margins"></div>
                  <input type="hidden" name="pregunta[]" value="'.$pregunta->id_pregunta.'">
               <h5>'.$pregunta->c_titulo_pregunta.'</h5> <div class="input-style input-style-1 input-required">
                <em>(requerido)</em>
                <input type="text" name="respuesta[]" placeholder="Porfavor llene este campo">
            </div>';
                }
                if ($pregunta->c_tipo_pregunta=='Numerico') {
                  echo'<div class="divider divider-margins"></div>
                  <input type="hidden" name="pregunta[]" value="'.$pregunta->id_pregunta.'">
               <h5>'.$pregunta->c_titulo_pregunta.'</h5> <div class="input-style input-style-1 input-required">
                <em>(requerido)</em>
                <input type="number" name="respuesta[]" step="any" placeholder="Porfavor llene este campo">
            </div>';
                }
                  if ($pregunta->c_tipo_pregunta=='Fecha') {
                  echo'<div class="divider divider-margins"></div>
                  <input type="hidden" name="pregunta[]" value="'.$pregunta->id_pregunta.'">
               <h5>'.$pregunta->c_titulo_pregunta.'</h5> <div class="input-style input-style-1 input-required">
                <em>(requerido)</em>
                <input type="date" name="respuesta[]">
            </div>';
                }
                      if ($pregunta->c_tipo_pregunta=='Hora') {
                  echo'<div class="divider divider-margins"></div>
                  <input type="hidden" name="pregunta[]" value="'.$pregunta->id_pregunta.'">
               <h5>'.$pregunta->c_titulo_pregunta.'</h5> <div class="input-style input-style-1 input-required">
                <em>(requerido)</em>
                <input type="time" name="respuesta[]">
            </div>';
                }
                         if ($pregunta->c_tipo_pregunta=='Hora') {
                  echo'<div class="divider divider-margins"></div>
                  <input type="hidden" name="pregunta[]" value="'.$pregunta->id_pregunta.'">
               <h5>'.$pregunta->c_titulo_pregunta.'</h5> <div class="input-style input-style-1 input-required">
                <em>(requerido)</em>
                <input type="datetime-local" name="respuesta[]">
            </div>';
                }
 if ($pregunta->c_tipo_pregunta=='Separador') {
                  echo'
                  <div class="divider divider-margins"></div><h4>'.$pregunta->c_titulo_pregunta.'</h4><br><div class="divider-icon bg-blue2-dark"><i class="fa font-17 color-blue2-dark fa fa-th-large"></i></div>';
                }

                 ?>
            <?php endforeach; ?>
            <div class="divider divider-margins"></div>
            
             <input type="submit" value="Guardar" name="submit" class="button button-m shadow-small button-circle bg-blue2-dark btnsubir">
        </div>
        </form>
	<?php include('includes/footer.php');?>
</div>
</body>
</html>