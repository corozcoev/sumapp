<?php
require_once "Controllers/conexion.php";
//session_start() crea una sesión o reanuda la actual basada en un identificador de sesión pasado mediante una petición GET o POST, o pasado mediante una cookie. 
session_start();
//$_SESSION es un array asociativo que contiene variables de sesión disponibles
if(!isset($_SESSION['admin']) || $_SESSION['estado'] != "conectado"){
  header('Location: index.php'); 
}
?>
<!-- desde la linea 17 a la 20 se llaman librerias como bootstrap y los font style que estan en la carpeta Views. En esta carpeta tambien se encuentran estilos css. Las imagenes e iconos estan en al carpeta img-->
<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>SuMapp</title>
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <link rel="stylesheet" href="Views/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link rel="stylesheet" href="Views/bower_components/font-awesome/css/font-awesome.min.css">
  
  <link rel="stylesheet" href="Views/dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="Views/dist/css/skins/skin-blue.min.css">
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <link href="Views/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <link href="//www.fuelcdn.com/fuelux/3.13.0/css/fuelux.min.css" rel="stylesheet">
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css" /> 
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
  <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.css"/>
 
<script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.10.21/datatables.min.js"></script>
  <style type="text/css">
  table{
      border: #e0e0e2 1px solid;
      border-radius: 20px;
    }
    thead{
      background: #e0e0e2;
    }
    tr:hover{
      background-image: radial-gradient(#a7a7a7, #e1e1e1) !important;
    }

    tr{
      transition: background-image 2s linear 1s;
    }
    .d-block{
      display: block;
    }
  </style>
</head>
<body class="hold-transition skin-blue sidebar-mini">
  
<div class="wrapper">
  <header class="main-header">
    <a class="logo">
      <span class="logo-mini"><b>S</b></span>
      <span class="logo-lg"><b>SuMapp</b></span>
    </a>
    <nav class="navbar navbar-static-top" role="navigation">
      <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
        <span class="sr-only">Toggle navigation</span>
      </a>
      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              <img src="Views/dist/img/avatar5.png" class="user-image" alt="User Image">
              <span class="hidden-xs"><?php echo $_SESSION['nombre'] ?></span>
            </a>
            <ul class="dropdown-menu">
              <li class="user-header">
                <img src="Views/dist/img/avatar5.png" class="img-circle" alt="User Image">
                <p>
                 <?php echo $_SESSION['nombre'] ?>
                </p>
                <p>Administrador</p>
              </li>
              <li class="user-footer">
                <div class="pull-left">
                  <a href="#" class="btn btn-default btn-flat">Perfil</a>
                </div>
                <div class="pull-right">
                  <a href="Controllers/cerrar_sesion.php" class="btn btn-default btn-flat">Salir</a>
                </div>
              </li>
            </ul>
          </li>
        </ul>
      </div>
    </nav>
  </header>
  <aside class="main-sidebar">
    <section class="sidebar">
      <div class="user-panel">
        <div class="pull-left image">
          <img src="Views/dist/img/avatar5.png" class="img-circle" alt="User Image">
        </div>
        <div class="pull-left info">
          <p>Admin</p>
          <a href="#"><i class="fa fa-circle text-success"></i>En línea</a>
        </div>
      </div>
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">Tablero</li>
        <li class="active"><a href="Gestion.php"><i class="fa fa-rocket"></i> <span>Aplicaciones</span></a></li>
        <li class=""><a href="Listasapps.php"><i class="fa fa-list-alt"></i> <span>Listas</span></a></li>
         <li class=""><a href="PermisosListas.php"><i class="fa fa-unlock-alt"></i> <span>Permisos Listas</span></a></li>
           <li class=""><a href="Empresas.php"><i class="fa fa-map-o"></i> <span>Empresas</span></a></li>
         <li class=""><a href="Pisos.php"><i class="fa fa-building"></i> <span>Pisos</span></a></li>
         <li class=""><a href="Salas.php"><i class="fa fa-briefcase"></i> <span>Salas</span></a></li>
        <li class=""><a href="AsignacionPS.php"><i class="fa fa-plus-square"></i> <span>Asignacion P/S</span></a></li>
        <li class=""><a href="Usuarios.php"><i class="fa fa-users"></i> <span>Usuarios</span></a></li>

 
        <!--<li class=""><a href="Asignaciones.php"><i class="fa fa-user-plus"></i> <span>Asignaciones</span></a></li>-->
        <!--<li class=""><a href="Resultados.php"><i class="fa fa-table"></i> <span>Resultados</span></a></li>-->
      </ul>
    </section>
  </aside>