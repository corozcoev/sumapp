<?php

require_once "Controllers/conexion.php";

$page= $_GET['page'];

$query = "SELECT l.id_lista,l.c_nombre_lista,l.fecha_creacion,l.fecha_actualizacion,a.c_nombre_app FROM tb_lista l
INNER JOIN tb_app a ON a.id_app=l.app
 		  order by l.id_lista		
		  limit ".(10*($page-1)).",".(10*($page));
$data = array();
try {
	$resultado = mysqli_query($conexion,$query);
	while( $row = mysqli_fetch_assoc($resultado)){
	    $data[$row['id_lista']] = $row;
	}
	$resp['error']=false;	
} catch (Exception $e) {	
	$resp['error']=true;	
}

$resp['data']=$data;
echo json_encode($resp);

?>
