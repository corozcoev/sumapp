<?php include 'Encabezado.php' ?>
  <div class="content-wrapper">
    <div class="col-sm-12" style="background: #ecf0f5;">
      <section id="main-content">
      <section class="wrapper" style="background: none;">
        <br>
         <div id="row-app" class="row">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Gestion Apps :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                              <button type="button" class="btn btn-success" id="btn-ver-app"><i class="fa fa-bars"></i> Ver Apps <i class="preloader preloader-success hidden"></i></button>
                              <button type="button" class="btn btn-primary" id="btn-crear-app"><i class="fa fa-plus-circle"></i> Nueva App <i class="preloader preloader-info hidden"></i></button>
                            </div>
                         </div>
                    </div>
                  </fieldset>
                </form>
                <div class="row" id="row-apps" style="display: none;">
                  <div class="col-md-12">
                    <section style="border: 1px solid #e0e0e2;" class="panel">
                      <table class="table table-striped table-hover" id="tb-app">
                        <thead>
                          <tr>
                            <th>#No.</th>
                            <th>Nombre del app</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Cuestionarios</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
                <div class="row" id="pagination">
                </div>
              </div>
            </section>
          </div>
        </div>
        <div class="row" id="row-encuesta" style="display: none;">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Gestion Cuestionarios :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
            <div class="panel-body">
              <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                      <div class="row">
                          <div class="col-sm-4">
                              <div class="form-group">
                                <label for="">App:</label>
                                <input type="hidden" class="id_app" value="">
                                <input type="text" class="form-control nombre_app" disabled value="">
                              </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group">
                                 <div class="form-group">
                                     <label class="d-block">&nbsp;</label>
                                     <button type="button" class="btn btn-primary" id="btn-crear-encuesta"><i class="fa fa-plus-circle"></i> Nuevo Cuestionario <i class="preloader preloader-info hidden"></i></button>
                                 </div>
                               </div>
                            </div>
                        </div>
                    </fieldset>
               </form>
               <section style="border: 1px solid #e0e0e2;" class="panel">
                <table class="table table-striped table-advance table-hover" id="tb-encuesta">
                  <thead>
                    <tr>
                            <th>Nombre del cuestionario</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Bloques</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                 
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </section>
              </div>
            </section>
          </div>
        </div>
        <div class="row" id="row-bloque" style="display: none;">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Gestion Bloques :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
            <div class="panel-body">
              <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                      <div class="row">
                          <div class="col-sm-4">
                              <div class="form-group">
                                <label for="">Cuestionario:</label>
                                <input type="hidden" class="id_encuesta" value="">
                                <input type="text" class="form-control nombre_encuesta" disabled value="">
                              </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group">
                                 <div class="form-group">
                                     <label class="d-block">&nbsp;</label>
                                     <button type="button" class="btn btn-primary" id="btn-crear-bloque"><i class="fa fa-plus-circle"></i> Nuevo Bloque <i class="preloader preloader-info hidden"></i></button>
                                 </div>
                               </div>
                            </div>
                        </div>
                    </fieldset>
               </form>
               <section style="border: 1px solid #e0e0e2;" class="panel">
                <table class="table table-striped table-advance table-hover" id="tb-bloque">
                  <thead>
                    <tr>
                      <th>Orden</th>
                      <th>Nombre del Bloque</th>
                      <th>Valor</th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                      <th></th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </section>
              </div>
            </section>
          </div>
        </div>
        <div class="row" id="row-pregunta" style="display: none;">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel panel-info">
                <div class="panel-heading">.: Gestion Preguntas :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                    <fieldset>
                        <div class="row">
                            <div class="col-sm-4">
                                <div class="form-group">
                                  <label for="">Bloque:</label>
                                  <input type="hidden" class="id_bloque" value="">
                                  <input type="text" class="form-control nombre_bloque" disabled value="">
                                </div>
                             </div>
                             <div class="col-sm-4">
                                 <div class="form-group">
                                   <div class="form-group">
                                       <label class="d-block">&nbsp;</label>
                                       <button type="button" class="btn btn-primary" id="btn-crear-pregunta"><i class="fa fa-plus-circle"></i> Nueva Pregunta <i class="preloader preloader-info hidden"></i></button>
                                   </div>
                                 </div>
                              </div>
                          </div>
                      </fieldset>
                 </form>
              <section style="border: 1px solid #e0e0e2;" class="panel">
                <table class="table table-striped table-advance table-hover" id="tb-pregunta">
                  <thead>

                  <tr>
                    <th>Orden</th>
                    <th>Titulo de la Pregunta</th>
                    <th>Tipo Pregunta</th>
                    <th>Valor</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>

                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </section>
              </div>
            </section>
          </div>
        </div>
      </section>
    </section>
    </div>
  </div>
</div>
<div class="modal fade" id="modal-opcion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-modal-opcion">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Gestion Lista</h4>
          </div>
          <div class="modal-body">
            <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-opcion">
                <fieldset>
                    <div class="col-sm-12">
                        <div class="col-sm-4">
                            <div class="form-group">
                              <label for="">Pregunta:</label>
                              <input type="hidden" class="id_pregunta" value="">
                              <input type="hidden" class="idenc" value="">
                              <input type="text" class="form-control nombre_pregunta" disabled value="">
                            </div>
                         </div>

                          <?php 
             $sqllista = "SELECT * FROM tb_lista";
             $querylista = $conexion->query($sqllista);//Se ejecuta consulta
$arraylista= array(); // Array donde vamos a guardar los datos 
while($resultadolista = $querylista->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraylista[]=$resultadolista; // Guardar los resultados en la variable
}
                 ?>
                  <div class="col-sm-4" style="margin-left: 30px;">
                            <div class="form-group">
                              <label for="fono2">Lista</label>
                <select class="form-control idlista" name="idlista" required>
                  <option disabled selected>Selecciona una opcion</option>
             <?php foreach ($arraylista as $l): ?>
                  <option value="<?php echo $l->id_lista ?>"><?php echo $l->c_nombre_lista ?></option>
                  <?php endforeach ?>
                </select>
                            </div>
                         </div>



                         <div class="col-sm-2" style="margin-left: 30px;">
                             <div class="form-group">
                               <div class="form-group">
                                   <label class="d-block">&nbsp;</label>
                                   <button type="button" class="btn btn-primary" id="sbmt-crear-opcion"><i class="fa fa-plus-circle"></i> Guardar <i class="preloader preloader-info hidden"></i></button>
                               </div>
                             </div>
                          </div>
                      </div>
                  </fieldset>
             </form>
          <section style="border: 1px solid #e0e0e2;" class="panel">
            <table class="table table-striped table-advance table-hover" id="tb-opcion">
              <thead>

              <tr>
              
                <th>Lista seleccionada</th>
      

              </tr>
              </thead>
              <tbody>

              </tbody>
            </table>
          </section>
          </div>
        </form>
      </div>
    </div>
</div>
<div class="modal fade" id="crear-app" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-app">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nueva App</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Nombre del app</label>
                <input class="form-control nomapp" name="nomapp" type="text" required >
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-app">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="crear-encuesta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-encuesta">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nuevo Cuestionario</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Nombre del Cuestionario</label>
                <input class="form-control nomenc" name="nomenc" type="text" required >
                <input type="hidden" class="idapp" value="">
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-encuesta">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="crear-bloque" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-bloque">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nuevo Bloque</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre de la Encuesta</label>
                   <input class="form-control nomenc" name="nomenc" type="text" disabled>
                   <input type="hidden" class="idenc" value="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre del bloque</label>
                  <input class="form-control nomblo" name="nomenc" type="text" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Respuesta predeterminada:</label>
                    <br>
<input type="radio" class="resp" id="si" name="resp" value="1" checked>
<label for="si">Si</label><br>
<input type="radio" class="resp" id="no" name="resp" value="2">
<label for="no">No</label><br>
<input type="radio" class="resp" id="ninguno" name="resp" value="ninguno">
<label for="ninguno">Ninguno</label>
                </div>
              </div>
               <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Valor de bloque</label>
                  <input class="form-control valblo" name="valblo" type="number" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Orden del Bloque</label>
                    <div id="spinner1">
                      <div class="input-group input-small">
                            <input class="spinner-input form-control" maxlength="3" readonly="" type="text">
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                                <button type="button" class="btn spinner-up btn-xs btn-default">
                                    <i class="fa fa-angle-up"></i>
                                </button>
                                <button type="button" class="btn spinner-down btn-xs btn-default">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-bloque">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="editar-app" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-app">
          <input type="hidden" class="idapp">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar App</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre del app</label>
                  <input class="form-control nomapp" name="nomapp" type="text" required >
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-app">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="editar-encuesta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-encuesta">
          <input type="hidden" class="idenc">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Cuestionario</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">App</label>
                  <input class="form-control nomapp" name="nomapp" type="text" disabled>
                </div>
                <div class="col-sm-12">
                    <label for="fono1">Nombre del cuestionario</label>
                  <input class="form-control nomenc" name="nomapp" type="text" required >
                </div>
               
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-encuesta">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>


<div class="modal fade" id="editar-bloque" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-bloque">
          <input type="hidden" class="idblo">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Bloque</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Encuesta</label>
                  <input class="form-control nomenc" name="nomenc" type="text" disabled>
                </div>
                <div class="col-sm-12">
                    <label for="fono1">Nombre del bloque</label>
                  <input class="form-control nomblo" name="nomenc" type="text" required >
                </div>
                     <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Respuesta predeterminada:</label>
                    <br>
<input type="radio" class="resp" id="si" name="respe" value="1" checked>
<label for="si">Si</label><br>
<input type="radio" class="resp" id="no" name="respe" value="2">
<label for="no">No</label><br>
<input type="radio" class="resp" id="ninguno" name="respe" value="ninguno">
<label for="ninguno">Ninguno</label>
                </div>
              </div>
                 <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Valor de bloque</label>
                  <input class="form-control valblo" name="valblo" type="number" required >
                </div>
              </div>
                <div class="col-sm-12">
                    <label for="fono1">Orden del Bloque</label>
                    <div id="spinner2">
                      <div class="input-group input-small">
                            <input class="spinner-input form-control" maxlength="3" readonly="" type="text">
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                                <button type="button" class="btn spinner-up btn-xs btn-default">
                                    <i class="fa fa-angle-up"></i>
                                </button>
                                <button type="button" class="btn spinner-down btn-xs btn-default">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-bloque">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="crear-pregunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-pregunta">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nueva Pregunta</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre del Cuestionario</label>
                   <input class="form-control nomenc" name="nomenc" type="text" disabled>
                   <input type="hidden" class="idenc" value="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre del Bloque</label>
                   <input class="form-control nomblo" name="nomblo" type="text" disabled>
                   <input type="hidden" class="idblo" value="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Titulo de la pregunta</label>
                  <input class="form-control nompreg" name="nompreg" type="text" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Orden de la pregunta</label>
                    <div id="spinner3">
                      <div class="input-group input-small">
                            <input class="spinner-input form-control" maxlength="3" type="text">
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                                <button type="button" class="btn spinner-up btn-xs btn-default">
                                    <i class="fa fa-angle-up"></i>
                                </button>
                                <button type="button" class="btn spinner-down btn-xs btn-default">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Tipo de pregunta</label>
                    <select name="tipopreg" id="tipopreg" class='form-control'>
                        <option value="RadioButton" disabled  selected="selected">Selecciona una opcion</option> 
                      <option value="RadioButton-Ev">RadioButton(Ev)</option> 
                      <option value="RadioButton-Ev-Com">RadioButton(Ev-Comentario)</option> 
                      <option value="RadioButton">RadioButton</option>
                      <option value="Texto">Texto</option>
                      <option value="Numerico">Numerico</option>
                      <option value="Lista">Lista</option> 
                       <option value="Separador">Separador</option>
                        <option value="Fecha">Fecha</option>
                      <option value="Hora">Hora</option>
                      <option value="Fecha-Hora">Fecha y Hora</option>                                                   
                    </select>
                </div>
              </div>
                 <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Valor de pregunta</label>
                  <input class="form-control valpre" name="valpre" type="number" required >
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-pregunta">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="editar-pregunta" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-pregunta">
          <input type="hidden" class="idpreg" value="">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Pregunta</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre de la Encuesta</label>
                   <input class="form-control nomenc" name="nomenc" type="text" disabled>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre del Bloque</label>
                   <input class="form-control nomblo" name="nomblo" type="text" disabled>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Titulo de la pregunta</label>
                  <input class="form-control nompreg" name="nompreg" type="text" required >
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Orden de la pregunta</label>
                    <div id="spinner4">
                      <div class="input-group input-small">
                            <input class="spinner-input form-control" maxlength="3"  type="text">
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                                <button type="button" class="btn spinner-up btn-xs btn-default">
                                    <i class="fa fa-angle-up"></i>
                                </button>
                                <button type="button" class="btn spinner-down btn-xs btn-default">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Tipo de pregunta</label>
                     <select name="tipopregedi" id="tipopregedi" class='form-control'>
                        <option value="RadioButton" disabled  selected="selected">Selecciona una opcion</option> 
                      <option value="RadioButton-Ev">RadioButton(Ev)</option> 
                      <option value="RadioButton-Ev-Com">RadioButton(Ev-Comentario)</option> 
                      <option value="RadioButton">RadioButton</option>
                      <option value="Texto">Texto</option>
                      <option value="Numerico">Numerico</option>
                      <option value="Lista">Lista</option> 
                      <option value="Fecha">Fecha</option>
                      <option value="Hora">Hora</option>
                      <option value="Fecha-Hora">Fecha y Hora</option>                                                    
                    </select>
                </div>
              </div>
                  <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Valor de pregunta</label>
                  <input class="form-control valpre" name="valpre" type="number" required >
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-pregunta">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="crear-opcion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-opcion">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nueva Opcion</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre de la Encuesta</label>
                   <input class="form-control nomenc" name="nomenc" type="text" disabled>
                   <input type="hidden" class="idenc" value="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre del Bloque</label>
                   <input class="form-control nomblo" name="nomblo" type="text" disabled>
                   <input type="hidden" class="idblo" value="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Titulo de la Pregunta</label>
                   <input class="form-control nompreg" name="nompreg" type="text" disabled>
                   <input type="hidden" class="idpreg" value="">
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Detalle de la Opcion</label>
                  <input class="form-control detopc" name="detopc" type="text" required >
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Orden de la Opcion</label>
                    <div id="spinner5">
                      <div class="input-group input-small">
                            <input class="spinner-input form-control" maxlength="3" readonly="" type="text">
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                                <button type="button" class="btn spinner-up btn-xs btn-default">
                                    <i class="fa fa-angle-up"></i>
                                </button>
                                <button type="button" class="btn spinner-down btn-xs btn-default">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Valor de la Opcion</label>
                    <div id="spinner6">
                      <div class="input-group input-small">
                            <input class="spinner-input form-control" maxlength="3" readonly="" type="text">
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                                <button type="button" class="btn spinner-up btn-xs btn-default">
                                    <i class="fa fa-angle-up"></i>
                                </button>
                                <button type="button" class="btn spinner-down btn-xs btn-default">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Tipo de pregunta</label>
                    <select name="tipoopc" id="tipoopc" class='form-control'>
                      <option value="OS" selected="selected">Opcion Simple</option>                        
                    </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-opcion">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="editar-opcion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-opcion">
          <input type="hidden" class="idopc" value="">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Opcion</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre de la Encuesta</label>
                   <input class="form-control nomenc" name="nomenc" type="text" disabled>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre del Bloque</label>
                   <input class="form-control nomblo" name="nomblo" type="text" disabled>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Titulo de la Pregunta</label>
                   <input class="form-control nompreg" name="nompreg" type="text" disabled>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Detalle de la Opcion</label>
                  <input class="form-control detopc" name="detopc" type="text" required >
                </div>
              </div>

              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Orden de la Opcion</label>
                    <div id="spinner8">
                      <div class="input-group input-small">
                            <input class="spinner-input form-control" maxlength="3" readonly="" type="text">
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                                <button type="button" class="btn spinner-up btn-xs btn-default">
                                    <i class="fa fa-angle-up"></i>
                                </button>
                                <button type="button" class="btn spinner-down btn-xs btn-default">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Valor de la Opcion</label>
                    <div id="spinner9">
                      <div class="input-group input-small">
                            <input class="spinner-input form-control" maxlength="3" readonly="" type="text">
                            <div class="spinner-buttons input-group-btn btn-group-vertical">
                                <button type="button" class="btn spinner-up btn-xs btn-default">
                                    <i class="fa fa-angle-up"></i>
                                </button>
                                <button type="button" class="btn spinner-down btn-xs btn-default">
                                    <i class="fa fa-angle-down"></i>
                                </button>
                            </div>
                        </div>
                    </div>
                </div>
              </div>
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Tipo de pregunta</label>
                    <select name="tipoopc" id="tipoopc" class='form-control'>
                      <option value="OS" selected="selected">Opcion Simple</option>                    
                    </select>
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-opcion">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal" id="cargando" data-backdrop="static" style="top:40%">
  <div class="modal-dialog" style="width: 155px;">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="loader.gif"></center>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
<script src="Views/bower_components/fuelux/spinner.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- DESDE AQUI COMIENZA EL CODIGO JS QUE SE EJECUTA CUANDO LE DAS CLICKS A LOS BOTONES. ES JQUERY BASICO -->
<script>
  $(function(){
      $('#row-apps').hide();
   
      $('#row-bloque').hide();
      $('#row-pregunta').hide();
      $('#row-encuesta').hide();
      $('#spinner1').spinner();
      $('#spinner2').spinner();
      $('#spinner3').spinner();
      $('#spinner4').spinner();
      $('#spinner5').spinner();
      $('#spinner6').spinner();
      $('#spinner7').spinner();
      $('#spinner8').spinner();
      $('#spinner9').spinner();
      $('#spinner10').spinner();
    ////////////////////////////////////////////////////////
      $('#btn-ver-app').click(function(){
         listarApps(1);
      });
    
      /////////////////////////////////////////////////////////
      function listarApps(page){
        $('#row-encuesta').hide();
        $('#row-bloque').hide();
        $('#row-pregunta').hide();
        $('#cargando').modal();
        var data={};
        data['page']=page;
        //SE OBTIENEN LAS ENCUESTAS HACIENDO LLAMADA AL ARCHIVO LISTAR_ENCUESTA.PHP. LA RESPUESTA ES LA VARIABLE RESP
        $.get('Listar_App.php',data,function(resp){
          $('#tb-app tbody').empty();
          $('#pagination').empty();
          var i=0;
          $.each(resp.data,function(key,App){
            var tr = '';
            i++;
            tr += '<tr id="app-'+ App.id_app +'">';

            tr += '<td width="5%">' + i + '</td>';
            tr += '<td width="40%">' + App.c_nombre_app + '</td>';
            tr += '<td width="15%">' + App.fecha_creacion + '</td>';
            tr += '<td width="15%">' + App.fecha_actualizacion + '</td>';
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-info btn-encuestas-app ttip" data-idapp="' + App.id_app + '" data-placement="top" data-toggle="tooltip" title="Gestionar cuestionarios"><i class="fa fa-align-justify"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-warning btn-editar-app ttip" data-idapp="' + App.id_app + '" data-placement="top" data-toggle="tooltip" title="Editar App"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
            if(App.c_tipo_app=='D'){
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-primary btn-gestionar-encuesta ttip" data-idapp="' + App.id_app + '" data-nomapp="'+App.c_nombre_app +'" data-placement="top" data-toggle="tooltip" title=""><i class="fa fa-user fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';

            }
            tr += '<td>';
                      tr += '<button class="btn btn-xs btn-danger btn-eliminar-app ttip" data-idapp="' + App.id_app + '" data-placement="top" data-toggle="tooltip" title="Borrar App"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
            tr += '</td>';
            tr += '</tr>';
            $('#tb-app tbody').append(tr);
          });
          $('#tb-app tbody .ttip').tooltip();
          $('#pagination').append(resp.links);
          $('#cargando').modal('toggle');
          $('#row-apps').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });

      };
      ////////////////////////////////////////////////////////
      $('#btn-crear-app').click(function(){
         $('#crear-app').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-app').click(function(){
        if( $('#crear-app .nomapp').val() == ''){
          toastr.error('DEBE INGRESAR EL NOMBRE');
          return false;
        }
        var data = {};
        data['nomapp'] = $('#crear-app .nomapp').val();

        $('#crear-app').modal('toggle');
        $('#cargando').modal();
        $.post('Agregar_App.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            $('#crear-app').modal('');
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#crear-app .nomapp').val('');
            toastr.success('APP AGREGADA CORRECTAMENTE');
            listarApps(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ////////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-encuesta',function(){
        var id = $(this).data('idencuesta');
        var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Eliminar_Encuesta.php',data,function(resp){
          $('#cargando').modal('toggle');
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#cargando').modal('toggle');
            toastr.success('CUESTIONARIO ELIMINADO CORRECTAMENTE');
            listarEncuestas($('#row-encuesta .id_app').val());
          }
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ////////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-bloque',function(){
        var id = $(this).data('idbloque');
        var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Eliminar_Bloque.php',data,function(resp){
          $('#cargando').modal('toggle');
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#cargando').modal('toggle');
            toastr.success('BLOQUE ELIMINADO CORRECTAMENTE');
            listarBloques($('#row-bloque .id_encuesta').val());
          }
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ////////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-pregunta',function(){
        var id = $(this).data('idpregunta');
        var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Eliminar_Pregunta.php',data,function(resp){
          $('#cargando').modal('toggle');
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#cargando').modal('toggle');
            toastr.success('PREGUNTA ELIMINADA CORRECTAMENTE');
            listarPreguntas($('#row-pregunta .id_bloque').val());
          }
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      ////////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-opcion',function(){
        var id = $(this).data('idopcion');
        var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Eliminar_Opcion.php',data,function(resp){
          $('#cargando').modal('toggle');
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#cargando').modal('toggle');
            toastr.success('OPCION ELIMINADO CORRECTAMENTE');
            listarOpciones($('#modal-opcion .id_pregunta').val());
          }
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      ////////////////////////////////////////////////////////////////////////////

      $(document).on('click','.btn-editar-app',function(){
        var id = $(this).data('idapp');
        var data = {};
        data['id'] = id;
        $.post('Obtener_App.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-app .idapp').val(resp.data.id_app);
            $('#f-editar-app .nomapp').val(resp.data.c_nombre_app);
            $('#f-editar-app #tipoapp').val(resp.data.c_tipo_app);
            $('#editar-app').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-app').click(function(){
        if( $('#f-editar-app .nomapp').val() == '0'){
          toastr.error('DEBE INGRESAR UN NOMBRE');
          return false;
        }
        if( $('#f-editar-app #tipoapp').val() == '0'){
          toastr.error('DEBE INGRESAR UN TIPO');
          return false;
        }
        var data = {};
        data['idapp'] = $('#f-editar-app .idapp').val();
        data['nomapp'] = $('#f-editar-app .nomapp').val();
        $.post('Actualizar_App.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-app').modal('toggle');
            toastr.success('APP ACTUALIZADA CORRECTAMENTE');
            listarApps(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ///////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-app',function(){
        var id = $(this).data('idapp');
        swal({
            title: "",
            text: "¿Esta seguro que desea eliminar el app?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) {
              var data = {};
              data['id'] = id;
              $.post('Eliminar_App.php',data,function(resp){
                if(resp.error){
                  toastr.error('ERROR: ' + resp.message);
                }else{
                  toastr.success('APP ELIMINADA CORRECTAMENTE');
                  listarApps(1);
                }

              },'json').fail(function(){
                toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
              });

            }
        });

      });
       ////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-encuestas-app',function(){
        var id = $(this).data('idapp');
        if( id == ''){
          toastr.error('DEBE SELECCIONAR EL TIPO');
          return false;
        }

        listarEncuestas(id);
      });
      /////////////////////////////////////////////////////////
      function listarEncuestas(id){
             var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Listar_Encuesta.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#row-encuesta .nombre_app').val(resp.app.c_nombre_app);
            $('#row-encuesta .id_app').val(resp.app.id_app);
            $('#tb-encuesta tbody').empty();
            $('#row-bloque').hide();
            $('#row-pregunta').hide();
            $.each(resp.data,function(key,Encuesta){
             var tr = '';
              tr += '<tr data-idencuesta="'+ Encuesta.id_encuesta +'">';

             
              tr += '<td width="30%">' + Encuesta.c_nombre_encuesta + '</td>';
              tr += '<td>' + Encuesta.fecha_creacion + '</td>';
              tr += '<td>' + Encuesta.fecha_actualizacion + '</td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-info btn-bloques-encuesta ttip" data-idencuesta="' + Encuesta.id_encuesta + '" data-placement="top" data-toggle="tooltip" title="Gestionar bloques"><i class="fa fa-align-justify"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-warning btn-editar-encuesta ttip" data-idencuesta="' + Encuesta.id_encuesta + '" data-placement="top" data-toggle="tooltip" title="Editar Encuesta"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-danger btn-eliminar-encuesta ttip" data-idencuesta="' + Encuesta.id_encuesta + '" data-placement="top" data-toggle="tooltip" title="Borrar Encuesta"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '</tr>';
              $('#tb-encuesta tbody').append(tr);
            });
          }
          $('#tb-encuesta tbody .ttip').tooltip();
          $('#row-encuesta').show();
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      }
       //////////////////////////////////////////////////////////////////////
 
      $('#btn-crear-encuesta').click(function(){
        var idapp=$('#row-encuesta .id_app').val();
        var nomapp=$('#row-encuesta .nombre_app').val();
        $('#f-crear-encuesta .idapp').val(idapp);
        $('#f-crear-encuesta .nomapp').val(nomapp);
        $('#crear-encuesta').modal();
      });
      //////////////////////////////////////////////////
           $('#sbmt-crear-encuesta').click(function(){

        if( $('#crear-encuesta .nomenc').val() == '' ){
          toastr.error('DEBE INGRESAR EL NOMBRE');
          return false;
        }
        var data = {};
        data['idapp'] = $('#crear-encuesta .idapp').val();
        data['nomenc'] = $('#crear-encuesta .nomenc').val();
        console.log(data['idapp']);
         console.log(data['nomenc']);
        $('#crear-encuesta').modal('toggle');

        $.post('Agregar_Encuesta.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
              $('#crear-encuesta').modal('');
          }else{
            $('#crear-encuesta .nomenc').val('');
            listarEncuestas( $('#crear-encuesta .idapp').val())
            toastr.success('CUESTIONARIO AGREGADO CORRECTAMENTE');
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////
      $(document).on('click','.btn-editar-encuesta',function(){
        var id = $(this).data('idencuesta');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Encuesta.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-encuesta .nomapp').val($('#row-encuesta .nombre_app').val());
            $('#f-editar-encuesta .idenc').val(resp.data.id_encuesta);
            $('#f-editar-encuesta .nomenc').val(resp.data.c_nombre_encuesta);
            $('#editar-encuesta').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //
      ////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-encuesta').click(function(){
        if( $('#editar-encuesta .nomenc').val() == '' ){
          toastr.error('DEBE INGRESAR EL NOMBRE');
          return false;
        }
        var data = {};
        data['idenc'] = $('#editar-encuesta .idenc').val();
        data['nomenc'] = $('#editar-encuesta .nomenc').val();

        $('#editar-encuesta').modal('toggle');

        $.post('Actualizar_Encuesta.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
              $('#editar-encuesta').modal('');
          }else{
            listarEncuestas( $('#row-encuesta .id_app').val())
            toastr.success('CUESTIONARIO ACTUALIZADO CORRECTAMENTE');
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       /////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-bloques-encuesta',function(){
        var id = $(this).data('idencuesta');
        if( id == ''){
          toastr.error('DEBE SELECCIONAR EL TIPO');
          return false;
        }

        listarBloques(id);
      });
      function listarBloques(id){

        var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Listar_Bloques.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#row-bloque .nombre_encuesta').val(resp.encuesta.c_nombre_encuesta);
            $('#row-bloque .id_encuesta').val(resp.encuesta.id_encuesta);
            $('#tb-bloque tbody').empty();
            $('#row-pregunta').hide();
            $.each(resp.data,function(key,Bloque){
              var tr = '';
              tr += '<tr data-idbloque="'+ Bloque.id_bloque +'">';
              tr += '<td width="5%">' + Bloque.n_orden_bloque  + '</td>';
              tr += '<td width="70%">' + Bloque.c_nombre_bloque + '</td>';
              tr += '<td width="5%">' + Bloque.valor  + '</td>';
              tr += '<td></td>';
              tr += '<td></td>';
              tr += '<td></td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-info btn-preguntas-encuesta ttip" data-idbloque="' + Bloque.id_bloque + '" data-placement="top" data-toggle="tooltip" title="Gestionar preguntas"><i class="fa fa-align-justify"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-warning btn-editar-bloque ttip" data-idbloque="' + Bloque.id_bloque + '" data-placement="top" data-toggle="tooltip" title="Editar Bloque"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-danger btn-eliminar-bloque ttip" data-idbloque="' + Bloque.id_bloque + '" data-placement="top" data-toggle="tooltip" title="Borrar Bloque"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '</tr>';
              $('#tb-bloque tbody').append(tr);
            });
          }
          $('#tb-bloque tbody .ttip').tooltip();
          $('#row-bloque').show();
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      }
      //////////////////////////////////////////////////////////////////////
      $('#btn-crear-bloque').click(function(){
        var idenc=$('#row-bloque .id_encuesta').val();
        var nomenc=$('#row-bloque .nombre_encuesta').val();
        var valblo=$('#row-bloque .valblo').val();
        var resp=$('input:radio[name=resp]:checked').val();
             console.log(resp);
        $('#f-crear-bloque .idenc').val(idenc);
        $('#f-crear-bloque .nomenc').val(nomenc);
        $('#crear-bloque').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-bloque').click(function(){

        if( $('#crear-bloque .nomblo').val() == '' ){
          toastr.error('DEBE INGRESAR EL NOMBRE');
          return false;
        }
        var data = {};
        data['idenc'] = $('#crear-bloque .idenc').val();
        data['nomblo'] = $('#crear-bloque .nomblo').val();
        data['valblo'] = $('#crear-bloque .valblo').val();
        data['resp'] =$('input:radio[name=resp]:checked').val();
        data['numblo'] = $('#crear-bloque #spinner1 input').val();

        $('#crear-bloque').modal('toggle');

        $.post('Agregar_Bloque.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
              $('#crear-bloque').modal('');
          }else{
            $('#crear-bloque .nomblo').val('');
             $('#crear-bloque .valblo').val('');
            $('input:radio[name=resp]:checked').val();

            $('#crear-bloque #spinner1').spinner('value',1);
            listarBloques( $('#crear-bloque .idenc').val())
            toastr.success('BLOQUE AGREGADO CORRECTAMENTE');
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      ////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-editar-bloque',function(){
        var id = $(this).data('idbloque');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Bloque.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-bloque .nomenc').val($('#row-bloque .nombre_encuesta').val());
            $('#f-editar-bloque .idblo').val(resp.data.id_bloque);
            $('#f-editar-bloque .nomblo').val(resp.data.c_nombre_bloque);
             $('#f-editar-bloque .valblo').val(resp.data.valor);
            $('#f-editar-bloque  #spinner2').spinner('value',resp.data.n_orden_bloque);
           // $('#spinner2').change(resp.data.n_orden_bloque);
            $('#editar-bloque').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-bloque').click(function(){
        if( $('#editar-bloque .nomblo').val() == '' ){
          toastr.error('DEBE INGRESAR EL NOMBRE');
          return false;
        }
          if( $('#editar-bloque .valblo').val() == '' ){
          toastr.error('DEBE INGRESAR VALOR');
          return false;
        }
         if( $('input:radio[name=respe]:checked').val() == '' ){
          toastr.error('DEBE SELECCIONAR RESPUESTA');
          return false;
        }
        var data = {};
        data['idblo'] = $('#editar-bloque .idblo').val();
        data['nomblo'] = $('#editar-bloque .nomblo').val();
        data['valblo'] = $('#editar-bloque .valblo').val();
        data['respe'] = $('input:radio[name=respe]:checked').val();
        data['numblo'] = $('#editar-bloque  #spinner2 input').val();

        $('#editar-bloque').modal('toggle');

        $.post('Actualizar_Bloque.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
              $('#editar-bloque').modal('');
          }else{
            listarBloques( $('#row-bloque .id_encuesta').val())
            toastr.success('BLOQUE ACTUALIZADO CORRECTAMENTE');
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      /////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-preguntas-encuesta',function(){
        var id = $(this).data('idbloque');
        if( id == ''){
          toastr.error('DEBE SELECCIONAR EL TIPO');
          return false;
        }

        listarPreguntas(id);
      });
      //////////////////////////////////////////////////
      function listarPreguntas(id){
        var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Listar_Preguntas.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#row-pregunta .nombre_bloque').val(resp.bloque.c_nombre_bloque);
            $('#row-pregunta .id_bloque').val(resp.bloque.id_bloque);

            $('#tb-pregunta tbody').empty();
            $.each(resp.data,function(key,Pregunta){
              var tr = '';
              tr += '<tr data-idpregunta="'+ Pregunta.id_pregunta +'">';

              tr += '<td width="5%">' + Pregunta.n_orden_pregunta  + '</td>';
              tr += '<td>' + Pregunta.c_titulo_pregunta + '</td>';
              tr += '<td>' + Pregunta.c_tipo_pregunta + '</td>';
              tr += '<td>' + Pregunta.valor + '</td>';
              tr += '<td></td>';
              tr += '<td></td>';
              if(Pregunta.c_tipo_pregunta=='Lista'){
                tr += '<td>';
                          tr += '<button class="btn btn-xs btn-info btn-gestion-listas ttip" data-idpregunta="' + Pregunta.id_pregunta + '" data-placement="top" data-toggle="tooltip" title="Gestionar Listas"><i class="fa fa-align-justify"></i> <i class="preloader preloader-info hide"></i></button>';
                tr += '</td>';
              }else {
                tr += '<td></td>';
              }
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-warning btn-editar-pregunta ttip" data-idpregunta="' + Pregunta.id_pregunta + '" data-placement="top" data-toggle="tooltip" title="Editar Pregunta"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';

              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-danger btn-eliminar-pregunta ttip" data-idpregunta="' + Pregunta.id_pregunta + '" data-placement="top" data-toggle="tooltip" title="Borrar Pregunta"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '</tr>';
              $('#tb-pregunta tbody').append(tr);
            });
          }
          $('#tb-pregunta tbody .ttip').tooltip();
          $('#row-pregunta').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      }
      /////////////////////////////////////////////////////////////////////
      $('#btn-crear-pregunta').click(function(){
        var idenc=$('#row-bloque .id_encuesta').val();
        var nomenc=$('#row-bloque .nombre_encuesta').val();
        $('#f-crear-pregunta .idenc').val(idenc);
        $('#f-crear-pregunta .nomenc').val(nomenc);
        var idblo=$('#row-pregunta .id_bloque').val();
        var nomblo=$('#row-pregunta .nombre_bloque').val();
        $('#f-crear-pregunta .idblo').val(idblo);
        $('#f-crear-pregunta .nomblo').val(nomblo);
        $('#crear-pregunta').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-pregunta').click(function(){
        if( $('#crear-pregunta .nompreg').val() == '' ){
          toastr.error('DEBE INGRESAR EL NOMBRE');
          return false;
        }
        var data = {};
        data['idenc'] = $('#crear-pregunta .idenc').val();
        data['idblo'] = $('#crear-pregunta .idblo').val();
        data['nompreg'] = $('#crear-pregunta .nompreg').val();
        data['detpreg'] = $('#crear-pregunta .detpreg').val();
        data['numpreg'] = $('#crear-pregunta #spinner3 input').val();
        data['tipopreg'] = $('#crear-pregunta #tipopreg').val();
        data['valpre'] = $('#crear-pregunta .valpre').val();

        $('#crear-pregunta').modal('toggle');

        $.post('Agregar_Pregunta.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
              $('#crear-bloque').modal('');
          }else{
            $('#crear-pregunta .nompreg').val('');
            $('#crear-pregunta .detpreg').val('');
            $('#crear-pregunta #spinner3').spinner('value',1);
            $('#crear-pregunta #tipopreg').val('');
            $('#crear-pregunta .valpre').val('');
            listarPreguntas( $('#crear-pregunta .idblo').val())
            toastr.success('PREGUNTA AGREGADA CORRECTAMENTE');
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      /////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-editar-pregunta',function(){
        var id = $(this).data('idpregunta');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Pregunta.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-pregunta .nomenc').val($('#row-bloque .nombre_encuesta').val());
            $('#f-editar-pregunta .nomblo').val($('#row-pregunta .nombre_bloque').val());
            $('#f-editar-pregunta .idpreg').val(resp.data.id_pregunta);
            $('#f-editar-pregunta .nompreg').val(resp.data.c_titulo_pregunta);
            $('#f-editar-pregunta #spinner4').spinner('value',resp.data.n_orden_pregunta);
            $('#f-editar-pregunta #tipopregedi').val(resp.data.c_tipo_pregunta);
             $('#f-editar-pregunta .valpre').val(resp.data.valor);
            $('#editar-pregunta').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-pregunta').click(function(){
        var tipopregedi=$('select[name="tipopregedi"]').val();
        if( $('#f-editar-pregunta .nompreg').val() == '0'){
          toastr.error('DEBE INGRESAR UN NOMBRE');
          return false;
        }
        if( $('#f-editar-pregunta .valpre').val() == '0'){
          toastr.error('DEBE INGRESAR UN VALOR');
          return false;
        }
        if( tipopregedi == '0'){
          toastr.error('DEBE INGRESAR UN TIPO');
          return false;
        }
        console.log(tipopregedi)
        var data = {};
        data['idpreg'] = $('#f-editar-pregunta .idpreg').val();
        data['nompreg'] =  $('#f-editar-pregunta .nompreg').val();
        data['detpreg'] =  $('#f-editar-pregunta .detpreg').val();
        data['numpreg'] =  $('#f-editar-pregunta #spinner4 input').val();
        data['tipopreg'] = tipopregedi;
        data['valpre'] =  $('#f-editar-pregunta .valpre').val();
        $.post('Actualizar_Pregunta.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-pregunta').modal('toggle');
            toastr.success('PREGUNTA ACTUALIZADA CORRECTAMENTE');
            listarPreguntas($('#row-pregunta .id_bloque').val());
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      /////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-gestion-listas',function(){
        var id = $(this).data('idpregunta');
        if( id == ''){
          toastr.error('DEBE SELECCIONAR EL TIPO');
          return false;
        }

        listarOpcionLista(id);
      });
      //////////////////////////////////////////////////
      function listarOpcionLista(id){
        var data = {};

        data['id'] = id;
        $('#cargando').modal();
        $.post('Listar_Opciones.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            return false;
          }else{
            $('#cargando').modal('toggle');
            var idenc=$('#row-bloque .id_encuesta').val();
            $('#modal-opcion .nombre_pregunta').val(resp.pregunta.c_titulo_pregunta);
            $('#modal-opcion .id_pregunta').val(resp.pregunta.id_pregunta);
            $('#modal-opcion .idenc').val(idenc);
            $('#tb-opcion tbody').empty();
            $.each(resp.data,function(key,Opcion){
              var tr = '';
              tr += '<tr data-idopcione="'+ Opcion.id_opcion_lista +'">';

              tr += '<td>' + Opcion.c_nombre_lista  + '</td>';
              tr += '</tr>';
              $('#tb-opcion tbody').append(tr);
            });
          }
          $('#tb-opcion tbody .ttip').tooltip();
          $('#modal-opcion').modal();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      }
  
      $('#sbmt-crear-opcion').click(function(){

        if( $('#modal-opcion .idlista').val() == '' ){
          toastr.error('DEBE INGRESAR EL NOMBRE');
          return false;
        }
        var data = {};
        data['idenc'] = $('#modal-opcion .idenc').val();
        data['idpreg'] = $('#modal-opcion .id_pregunta').val();
        data['idlista'] = $('select[name=idlista]').val();

        $('#modal-opcion').modal('toggle');
   console.log(data['idenc']);
   console.log(data['idpreg']);
   console.log(data['idlista']);

        $.post('Agregar_Opcion.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
            $('#modal-opcion').modal('');
          }else{
            toastr.success('LISTA ASIGNADA CORRECTAMENTE');
         
          

          }
        },'json').fail(function(){
         toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });

      //////////////////////////////////////////////////////////////////////////////
  });
</script>
</body>
</html>
