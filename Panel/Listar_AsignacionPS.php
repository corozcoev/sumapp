<?php

require_once "Controllers/conexion.php";

$page= $_GET['page'];

$query = "SELECT APS.id_asignacionps,A.c_nombre_app,E.c_nombre_encuesta,B.c_nombre_bloque,SU.sucursal,P.piso,S.sala,APS.qr,APS.fecha_creacion,APS.fecha_actualizacion FROM tb_asignacion_ps APS
INNER JOIN tb_app A ON A.id_app=APS.id_app
INNER JOIN tb_encuesta E ON E.id_encuesta=APS.id_cuestionario
INNER JOIN tb_encuesta_bloque B ON B.id_bloque=APS.id_bloque
INNER JOIN tb_sucursal SU ON SU.id_sucursal=APS.id_sucursal
INNER JOIN tb_piso P ON P.id_piso=APS.id_piso
INNER JOIN tb_sala S on S.id_sala=APS.id_sala
 		  order by APS.id_asignacionps		
		  limit ".(10*($page-1)).",".(10*($page));
$data = array();
try {
	$resultado = mysqli_query($conexion,$query);
	while( $row = mysqli_fetch_assoc($resultado)){
	    $data[$row['id_asignacionps']] = $row;
	}
	$resp['error']=false;	
} catch (Exception $e) {	
	$resp['error']=true;	
}

$resp['data']=$data;
echo json_encode($resp);

?>
