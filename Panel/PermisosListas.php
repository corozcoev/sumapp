<?php include 'Encabezado.php' ?>
  <div class="content-wrapper">
    <div class="col-sm-12" style="background: #ecf0f5;">
      <section id="main-content">
      <section class="wrapper" style="background: none;">
        <br>
         <div id="row-permiso" class="row">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.:Permisos Listas :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                              <button type="button" class="btn btn-success" id="btn-ver-permiso"><i class="fa fa-bars"></i> Ver Permisos<i class="preloader preloader-success hidden"></i></button>
                              <button type="button" class="btn btn-primary" id="btn-crear-permiso"><i class="fa fa-plus-circle"></i> Nuevo Permiso <i class="preloader preloader-info hidden"></i></button>
                            </div>
                         </div>
                    </div>
                  </fieldset>
                </form>
                <div class="row" id="row-permisos" style="display: none;">
                  <div class="col-md-12">
                    <section style="border: 1px solid #e0e0e2;" class="panel">
                      <table class="table table-striped table-hover" id="tb-permiso">
                        <thead>
                          <tr>
                            <th>Usuario</th>
                            <th>Lista</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
                <div class="row" id="pagination">
                </div>
              </div>
            </section>
          </div>
        </div>
    


      </section>
    </section>
    </div>
  </div>
</div>

<div class="modal fade" id="crear-permiso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-permiso">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nuevo Permiso</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                <?php 
$sqlusuario="SELECT id_usuario,nombre,apellido FROM tb_usuario ";
$queryusuario = $conexion->query($sqlusuario);//Se ejecuta consulta
$arrayusuario= array(); // Array donde vamos a guardar los datos 
while($resultadousuario = $queryusuario->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayusuario[]=$resultadousuario; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Usuario</label>
                <select name="usuario" class="form-control usuario">  
                 <option selected disabled>Selecciona usuario</option>
                 <?php foreach ($arrayusuario as $u):?>
                 <option value="<?php echo $u->id_usuario ?>"><?php echo $u->nombre.' '.$u->apellido ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>
                  <div class="form-group">
              <div class="col-sm-12">
                             <?php 
$sqllista="SELECT id_lista,c_nombre_lista FROM tb_lista ";
$querylista = $conexion->query($sqllista);//Se ejecuta consulta
$arraylista= array(); // Array donde vamos a guardar los datos 
while($resultadolista = $querylista->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraylista[]=$resultadolista; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Lista</label>
                 <select name="lista" class="form-control lista">  
                 <option selected disabled>Selecciona lista</option>
                 <?php foreach ($arraylista as $l):?>
                 <option value="<?php echo $l->id_lista ?>"><?php echo $l->c_nombre_lista ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>
          
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-permiso">Dar permisos <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editar-permiso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-permiso">
          <input type="hidden" class="idpermiso">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Permiso</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
                <div class="form-group">
              <div class="col-sm-12">
                <?php 
$sqlusuario="SELECT id_usuario,nombre,apellido FROM tb_usuario ";
$queryusuario = $conexion->query($sqlusuario);//Se ejecuta consulta
$arrayusuario= array(); // Array donde vamos a guardar los datos 
while($resultadousuario = $queryusuario->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayusuario[]=$resultadousuario; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Usuario</label>
                <select name="eusuario" class="form-control usuario">  
                 <option selected disabled>Selecciona usuario</option>
                 <?php foreach ($arrayusuario as $u):?>
                 <option value="<?php echo $u->id_usuario ?>"><?php echo $u->nombre.' '.$u->apellido ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>
                 <div class="form-group">
              <div class="col-sm-12">
                             <?php 
$sqllista="SELECT id_lista,c_nombre_lista FROM tb_lista ";
$querylista = $conexion->query($sqllista);//Se ejecuta consulta
$arraylista= array(); // Array donde vamos a guardar los datos 
while($resultadolista = $querylista->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraylista[]=$resultadolista; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Lista</label>
                 <select name="elista" class="form-control lista">  
                 <option selected disabled>Selecciona lista</option>
                 <?php foreach ($arraylista as $l):?>
                 <option value="<?php echo $l->id_lista ?>"><?php echo $l->c_nombre_lista ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>
                  
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-permiso">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>





<div class="modal" id="cargando" data-backdrop="static" style="top:40%">
  <div class="modal-dialog" style="width: 155px;">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="loader.gif"></center>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
<script src="Views/bower_components/fuelux/spinner.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- DESDE AQUI COMIENZA EL CODIGO JS QUE SE EJECUTA CUANDO LE DAS CLICKS A LOS BOTONES. ES JQUERY BASICO -->
<script>
  $(function(){
      $('#row-permisos').hide();
   


    ////////////////////////////////////////////////////////
      $('#btn-ver-permiso').click(function(){
         listarPermisos(1);
      });
    
      /////////////////////////////////////////////////////////
      function listarPermisos(page){
        $('#cargando').modal();
        var data={};
        data['page']=page;
        //SE OBTIENEN LAS ENCUESTAS HACIENDO LLAMADA AL ARCHIVO LISTAR_ENCUESTA.PHP. LA RESPUESTA ES LA VARIABLE RESP
        $.get('Listar_PermisoLista.php',data,function(resp){
          $('#tb-permiso tbody').empty();
          $('#pagination').empty();
          var i=0;
          $.each(resp.data,function(key,Permiso){
            var tr = '';
            i++;
            tr += '<tr id="permiso-'+ Permiso.id_permisolista +'">';
            tr += '<td >' + Permiso.nombre +' '+ Permiso.apellido +'</td>';
            tr += '<td >' + Permiso.c_nombre_lista + '</td>';
            tr += '<td >' + Permiso.fecha_creacion + '</td>';
            tr += '<td >' + Permiso.fecha_actualizacion + '</td>';
          
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-warning btn-editar-permiso ttip" data-idpermiso="' + Permiso.id_permisolista + '" data-placement="top" data-toggle="tooltip" title="Editar Permiso"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
   
            tr += '<td>';
                      tr += '<button class="btn btn-xs btn-danger btn-eliminar-permiso ttip" data-idpermiso="' + Permiso.id_permisolista + '" data-placement="top" data-toggle="tooltip" title="Borrar Permiso"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
            tr += '</td>';
            tr += '</tr>';
            $('#tb-permiso tbody').append(tr);
          });
          $('#tb-permiso tbody .ttip').tooltip();
          $('#pagination').append(resp.links);
          $('#cargando').modal('toggle');
          $('#row-permisos').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });

      };
      ////////////////////////////////////////////////////////
      $('#btn-crear-permiso').click(function(){
         $('#crear-permiso').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-permiso').click(function(){
        var usuario=$('select[name="usuario"]').val();
        var lista=$('select[name="lista"]').val();

        if( usuario == ''){
          toastr.error('DEBE SELECCIONAR USUARIO');
          return false;
        }

         if( lista == ''){
          toastr.error('DEBE SELECCIONAR LISTA');
          return false;
        }


    
        var data = {};
        data['usuario'] = usuario;
        data['lista'] = lista;
        
           
        $('#crear-permiso').modal('toggle');
        $('#cargando').modal();
        $.post('Agregar_PermisoLista.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            $('#crear-permiso').modal('');
            return false;
          }else{
            $('#cargando').modal('toggle');
        var usuario=$('select[name="usuario"]').val();
        var lista=$('select[name="lista"]').val();
       

            toastr.success('SE HAN PROPORCIONADO PERMISOS CORRECTAMENTE');
            listarPermisos(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       /////////////////////////////////////////////////////////////////////

      $(document).on('click','.btn-editar-permiso',function(){
        var id = $(this).data('idpermiso');
        var data = {};
        data['id'] = id;
        $.post('Obtener_PermisoLista.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-permiso .idpermiso').val(resp.data.id_permisolista);
            $('#f-editar-permiso .usuario').val(resp.data.id_usuario);
            $('#f-editar-permiso .lista').val(resp.data.id_lista);
            $('#editar-permiso').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-permiso').click(function(){
        var usuario=$('select[name="eusuario"]').val();
        var lista=$('select[name="elista"]').val();
       

        if( usuario == '0'){
          toastr.error('DEBE SELECCIONAR UN USUARIO');
          return false;
        }
        if( lista == '0'){
          toastr.error('DEBE SELECCIONAR UNA LISTA');
          return false;
        }
        var data = {};
        data['idpermiso'] = $('#f-editar-permiso .idpermiso').val();
        data['usuario'] = usuario;
        data['lista'] = lista;
        
       
        $.post('Actualizar_PermisoLista.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-permiso').modal('toggle');
            toastr.success('PERMISO ACTUALIZADO CORRECTAMENTE');
            listarPermisos(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ///////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-permiso',function(){
        var id = $(this).data('idpermiso');
        console.log(id);
        swal({
            title: "",
            text: "¿Esta seguro que desea eliminar este permiso?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) {
              var data = {};
              data['id'] = id;
              $.post('Eliminar_PermisoLista.php',data,function(resp){
                if(resp.error){
                  toastr.error('ERROR: ' + resp.message);
                }else{
                  toastr.success('PERMISO ELIMINADO CORRECTAMENTE');
                  listarPermisos(1);
                }

              },'json').fail(function(){
                toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
              });

            }
        });

      });
     
      /////////////////////////////////////////////////////////

      //////////////////////////////////////////////////////////////////////////////
  });
</script>
</body>
</html>
