<?php include 'Encabezado.php' ?>
  <div class="content-wrapper">
    <div class="col-sm-12" style="background: #ecf0f5;">
      <section id="main-content">
      <section class="wrapper" style="background: none;">
        <br>
         <div id="row-sucursal" class="row">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Gestion Sucursales :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                              <button type="button" class="btn btn-success" id="btn-ver-sucursal"><i class="fa fa-bars"></i> Ver Sucursales<i class="preloader preloader-success hidden"></i></button>
                              <button type="button" class="btn btn-primary" id="btn-crear-sucursal"><i class="fa fa-plus-circle"></i> Nueva Sucursal <i class="preloader preloader-info hidden"></i></button>
                            </div>
                         </div>
                    </div>
                  </fieldset>
                </form>
                <div class="row" id="row-sucursales" style="display: none;">
                  <div class="col-md-12">
                    <section style="border: 1px solid #e0e0e2;" class="panel">
                      <table class="table table-striped table-hover" id="tb-sucursal">
                        <thead>
                          <tr>
                            <th>Sucursal</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
                <div class="row" id="pagination">
                </div>
              </div>
            </section>
          </div>
        </div>
    


      </section>
    </section>
    </div>
  </div>
</div>

<div class="modal fade" id="crear-sucursal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-sucursal">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nueva Sucursal</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Sucursal</label>
                <input class="form-control nomsucursal" name="nomsucursal" type="text" required >
                <br>
  
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-sucursal">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editar-sucursal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-sucursal">
          <input type="hidden" class="idsucursal">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Sucursal</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Sucursal</label>
                  <input class="form-control nomsucursal" name="nomsucursal" type="text" required >
                                  
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-sucursal">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>





<div class="modal" id="cargando" data-backdrop="static" style="top:40%">
  <div class="modal-dialog" style="width: 155px;">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="loader.gif"></center>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
<script src="Views/bower_components/fuelux/spinner.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- DESDE AQUI COMIENZA EL CODIGO JS QUE SE EJECUTA CUANDO LE DAS CLICKS A LOS BOTONES. ES JQUERY BASICO -->
<script>
  $(function(){
      $('#row-sucursales').hide();
   


    ////////////////////////////////////////////////////////
      $('#btn-ver-sucursal').click(function(){
         listarSucursales(1);
      });
    
      /////////////////////////////////////////////////////////
      function listarSucursales(page){
        $('#cargando').modal();
        var data={};
        data['page']=page;
        //SE OBTIENEN LAS ENCUESTAS HACIENDO LLAMADA AL ARCHIVO LISTAR_ENCUESTA.PHP. LA RESPUESTA ES LA VARIABLE RESP
        $.get('Listar_Sucursal.php',data,function(resp){
          $('#tb-sucursal tbody').empty();
          $('#pagination').empty();
          var i=0;
          $.each(resp.data,function(key,Sucursal){
            var tr = '';
            i++;
            tr += '<tr id="sucursal-'+ Sucursal.id_sucursal +'">';
            tr += '<td >' + Sucursal.sucursal + '</td>';
            tr += '<td >' + Sucursal.fecha_creacion + '</td>';
            tr += '<td >' + Sucursal.fecha_actualizacion + '</td>';
          
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-warning btn-editar-sucursal ttip" data-idsucursal="' + Sucursal.id_sucursal + '" data-placement="top" data-toggle="tooltip" title="Editar Sucursal"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
        
            tr += '<td>';
                      tr += '<button class="btn btn-xs btn-danger btn-eliminar-sucursal ttip" data-idsucursal="' + Sucursal.id_sucursal + '" data-placement="top" data-toggle="tooltip" title="Borrar Sucursal"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
            tr += '</td>';
            tr += '</tr>';
            $('#tb-sucursal tbody').append(tr);
          });
          $('#tb-sucursal tbody .ttip').tooltip();
          $('#pagination').append(resp.links);
          $('#cargando').modal('toggle');
          $('#row-sucursales').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });

      };
      ////////////////////////////////////////////////////////
      $('#btn-crear-sucursal').click(function(){
         $('#crear-sucursal').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-sucursal').click(function(){
        if( $('#crear-sucursal .nomsucursal').val() == ''){
          toastr.error('DEBE INGRESAR EL NOMBRE DE LA SUCURSAL');
          return false;
        }
    
        var data = {};
        data['nomsucursal'] = $('#crear-sucursal .nomsucursal').val();
           
        $('#crear-sucursal').modal('toggle');
        $('#cargando').modal();
        $.post('Agregar_Sucursal.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            $('#crear-sucursal').modal('');
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#crear-sucursal .nomsucursal').val('');
            toastr.success('SUCURSAL AGREGADA CORRECTAMENTE');
            listarSucursales(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       /////////////////////////////////////////////////////////////////////

      $(document).on('click','.btn-editar-sucursal',function(){
        var id = $(this).data('idsucursal');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Sucursal.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-sucursal .idsucursal').val(resp.data.id_sucursal);
            $('#f-editar-sucursal .nomsucursal').val(resp.data.sucursal);
            $('#editar-sucursal').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-sucursal').click(function(){
        if( $('#f-editar-sucursal .nomsucursal').val() == '0'){
          toastr.error('DEBE INGRESAR NOMBRE DE SUCURSAL');
          return false;
        }
      
        var data = {};
        data['idsucursal'] = $('#f-editar-sucursal .idsucursal').val();
        data['nomsucursal'] = $('#f-editar-sucursal .nomsucursal').val();
       
        $.post('Actualizar_Sucursal.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-sucursal').modal('toggle');
            toastr.success('SUCURSAL ACTUALIZADA CORRECTAMENTE');
            listarSucursales(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ///////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-sucursal',function(){
        var id = $(this).data('idsucursal');
        swal({
            title: "",
            text: "¿Esta seguro que desea eliminar la sucursal?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) {
              var data = {};
              data['id'] = id;
              $.post('Eliminar_Sucursal.php',data,function(resp){
                if(resp.error){
                  toastr.error('ERROR: ' + resp.message);
                }else{
                  toastr.success('SUCURSAL ELIMINADA CORRECTAMENTE');
                  listarSucursales(1);
                }

              },'json').fail(function(){
                toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
              });

            }
        });

      });
     
      /////////////////////////////////////////////////////////

      //////////////////////////////////////////////////////////////////////////////
  });
</script>
</body>
</html>
