<?php include 'Encabezado.php' ?>
  <div class="content-wrapper">
    <div class="col-sm-12" style="background: #ecf0f5;">
      <section id="main-content">
      <section class="wrapper" style="background: none;">
        <br>
         <div id="row-usuario" class="row">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Gestion Usuarios:.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                              <button type="button" class="btn btn-success" id="btn-ver-usuario"><i class="fa fa-bars"></i> Ver Usuarios<i class="preloader preloader-success hidden"></i></button>
                              <button type="button" class="btn btn-primary" id="btn-crear-usuario"><i class="fa fa-plus-circle"></i> Nuevo Usuario <i class="preloader preloader-info hidden"></i></button>
                            </div>
                         </div>
                    </div>
                  </fieldset>
                </form>
                <div class="row" id="row-usuarios" style="display: none;">
                  <div class="col-md-12">
                    <section style="border: 1px solid #e0e0e2;" class="panel">
                      <table class="table table-striped table-hover" id="tb-usuario">
                        <thead>
                          <tr>
                            <th>Nombre</th>
                            <th>Apellidos</th>
                            <th>Correo</th>
                            <th>Contraseña</th>
                            <th>App</th>
                            <th>Empresa</th>
                            <th>Tipo</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
                <div class="row" id="pagination">
                </div>
              </div>
            </section>
          </div>
        </div>
    <script type="text/javascript">
     var idioma=

            {

                "sProcessing":     "Procesando...",
                "sLengthMenu":     "Mostrar _MENU_ registros",
                "sZeroRecords":    "No se encontraron resultados",
                "sEmptyTable":     "Ningun dato disponible en esta tabla",
                "sInfo":           "Mostrando registros del _START_ al _END_ de un total de _TOTAL_ registros",
                "sInfoEmpty":      "Mostrando registros del 0 al 0 de un total de 0 registros",
                "sInfoFiltered":   "(filtrado de un total de _MAX_ registros)",
                "sInfoPostFix":    "",
                "sSearch":         "Buscar dato especifico:",
                "sUrl":            "",
                "sInfoThousands":  ",",
                "sLoadingRecords": "Cargando...",
                "oPaginate": {
                    "sFirst":    "Primero",
                    "sLast":     "ÃƒÅ¡ltimo",
                    "sNext":     "Siguiente",
                    "sPrevious": "Anterior"
                },
                "oAria": {
                    "sSortAscending":  ": Activar para ordenar la columna de manera ascendente",
                    "sSortDescending": ": Activar para ordenar la columna de manera descendente"
                },
                "buttons": {
                    "copyTitle": 'Informacion copiada',
                    "copyKeys": 'Use your keyboard or menu to select the copy command',
                    "copySuccess": {
                        "_": '%d filas copiadas al portapapeles',
                        "1": '1 fila copiada al portapapeles'
                    },

                    "pageLength": {
                    "_": "Mostrar %d filas",
                    "-1": "Todo"
                    }
                }
            };

    $(document).ready(function() {
    $('#tb-usuario').DataTable( {
       "paging": true,
    "lengthChange": true,
    "searching": false,
    "ordering": true,
    "info": true,
    "autoWidth": true,
    "language": idioma,
    "lengthMenu": [[5,10,30,31, -1],[5,10,30,31,"Mostrar Todo"]]
    } );
} );
</script>


      </section>
    </section>
    </div>
  </div>
</div>

<div class="modal fade" id="crear-usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-usuario">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nuevo Usuario</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Nombre</label>
                <input class="form-control nomusu" name="nomusu" type="text" required >
                <br>
              </div>
              </div>
               <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono2">Apellidos</label>
                <input class="form-control apeusu" name="apeusu" type="text" required >
                <br>
              </div>
              </div>
               <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono3">Correo</label>
                <input class="form-control correo" name="correo" type="email" required >
                <br>
              </div>
              </div>
                <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono3">Contraseña</label>
                <input class="form-control password" name="password" type="password" required >
                <br>
              </div>
              </div>
               <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono4">Telefono</label>
                <input class="form-control tel" name="tel" type="number" required >
                <br>
              </div>
              </div>
                  <?php 
$sqlapp="SELECT * FROM tb_app ";
$queryapp = $conexion->query($sqlapp);//Se ejecuta consulta
$arrayapp= array(); // Array donde vamos a guardar los datos 
while($resultadoapp = $queryapp->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayapp[]=$resultadoapp; // Guardar los resultados en la variable

}
 ?>
  <div class="form-group">
              <div class="col-sm-12">
                   <label for="fono6">App </label>
                <select name="app" class="form-control app">  
                 <option selected disabled>Selecciona app</option>
                 <?php foreach ($arrayapp as $a):?>
                 <option value="<?php echo $a->id_app ?>"><?php echo $a->c_nombre_app ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
              </div>
              </div>
      <?php 
$sqlempresa="SELECT * FROM tb_empresa ";
$queryempresa = $conexion->query($sqlempresa);//Se ejecuta consulta
$arrayempresa= array(); // Array donde vamos a guardar los datos 
while($resultadoempresa= $queryempresa->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayempresa[]=$resultadoempresa; // Guardar los resultados en la variable

}
 ?>
  <div class="form-group">
              <div class="col-sm-12">
                   <label for="fono6">Empresa </label>
                <select name="empresa" class="form-control empresa" onchange="Obtenertipo(this)">  
                 <option selected disabled>Selecciona empresa</option>
                 <?php foreach ($arrayempresa as $e):?>
                 <option value="<?php echo $e->id_empresa ?>"><?php echo $e->c_nombre_empresa  ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
              </div>
              </div>
                <script type="text/javascript">
   function Obtenertipo(selectObject) {
    var empresa = selectObject.value;  

  $.ajax({
        data:{'empresa':empresa}, // Adjuntamos los parametros
        url:'Tipo_Cuenta.php', // ruta del archivo php que procesará nuestra solicitud
        type:  'get', // metodo por el cual se mandarán los datos
        beforeSend: function () { // callback que se ejecutará antes de enviar la solicitud
          console.log("Enviando por medio de post");
        },
        success:  function (response) { // callback que se ejecutará una vez el servidor responda
           $("#tipo").html(response);
        }
      });
    }


 </script>
   <div class="form-group">
              <div class="col-sm-12" id="tipo">
                
             
              </div>
              </div>
              <div class="form-group">
              <div class="col-sm-12" id="sucursal">
                
             
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-usuario">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editar-usuario" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-usuario">
          <input type="hidden" class="idusuario">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Usuario</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Nombre</label>
                <input class="form-control nomusu" name="nomusu" type="text" required >
                <br>
              </div>
              </div>
               <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono2">Apellidos</label>
                <input class="form-control apeusu" name="apeusu" type="text" required >
                <br>
              </div>
              </div>
               <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono3">Correo</label>
                <input class="form-control correo" name="correo" type="email" required >
                <br>
              </div>
              </div>
                <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono3">Contraseña</label>
                <input class="form-control password" name="password" type="password" required >
                <br>
              </div>
              </div>
               <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono4">Telefono</label>
                <input class="form-control tel" name="tel" type="phone" required >
                <br>
              </div>
              </div>
                    <?php 
$sqlapp="SELECT * FROM tb_app ";
$queryapp = $conexion->query($sqlapp);//Se ejecuta consulta
$arrayapp= array(); // Array donde vamos a guardar los datos 
while($resultadoapp = $queryapp->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayapp[]=$resultadoapp; // Guardar los resultados en la variable

}
 ?>
  <div class="form-group">
              <div class="col-sm-12">
                   <label for="fono5">App </label>
                <select name="eapp" class="form-control eapp">  
                 <option selected disabled>Selecciona app</option>
                 <?php foreach ($arrayapp as $a):?>
                 <option value="<?php echo $a->id_app ?>"><?php echo $a->c_nombre_app ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
              </div>
              </div>
   <?php 
$sqlempresa="SELECT * FROM tb_empresa ";
$queryempresa = $conexion->query($sqlempresa);//Se ejecuta consulta
$arrayempresa= array(); // Array donde vamos a guardar los datos 
while($resultadoempresa= $queryempresa->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayempresa[]=$resultadoempresa; // Guardar los resultados en la variable

}
 ?>
  <div class="form-group">
              <div class="col-sm-12">
                   <label for="fono6">Empresa </label>
                <select name="eempresa" class="form-control eempresa" onchange="EObtenertipo(this)">  
                 <option selected disabled>Selecciona empresa</option>
                 <?php foreach ($arrayempresa as $e):?>
                 <option value="<?php echo $e->id_empresa ?>"><?php echo $e->c_nombre_empresa  ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
              </div>
              </div>
                <script type="text/javascript">
   function EObtenertipo(selectObject) {
    var empresa = selectObject.value;  

  $.ajax({
        data:{'empresa':empresa}, // Adjuntamos los parametros
        url:'Editar_Tipo_Cuenta.php', // ruta del archivo php que procesará nuestra solicitud
        type:  'get', // metodo por el cual se mandarán los datos
        beforeSend: function () { // callback que se ejecutará antes de enviar la solicitud
          console.log("Enviando por medio de post");
        },
        success:  function (response) { // callback que se ejecutará una vez el servidor responda
           $("#etipo").html(response);
        }
      });
    }


 </script>
 <div id="etipo"></div>
 <div id="esucursal"></div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-usuario">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>





<div class="modal" id="cargando" data-backdrop="static" style="top:40%">
  <div class="modal-dialog" style="width: 155px;">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="loader.gif"></center>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
<script src="Views/bower_components/fuelux/spinner.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- DESDE AQUI COMIENZA EL CODIGO JS QUE SE EJECUTA CUANDO LE DAS CLICKS A LOS BOTONES. ES JQUERY BASICO -->
<script>
  $(function(){
      $('#row-usuarios').hide();
   


    ////////////////////////////////////////////////////////
      $('#btn-ver-usuario').click(function(){
         listarUsuarios(1);
      });
    
      /////////////////////////////////////////////////////////
      function listarUsuarios(page){
        $('#cargando').modal();
        var data={};
        data['page']=page;
        //SE OBTIENEN LAS ENCUESTAS HACIENDO LLAMADA AL ARCHIVO LISTAR_ENCUESTA.PHP. LA RESPUESTA ES LA VARIABLE RESP
        $.get('Listar_Usuario.php',data,function(resp){
          $('#tb-usuario tbody').empty();
          $('#pagination').empty();
          var i=0;
          $.each(resp.data,function(key,Usuario){
            var tr = '';
            i++;
            tr += '<tr id="usuario-'+ Usuario.id_usuario +'">';
            tr += '<td >' + Usuario.nombre + '</td>';
            tr += '<td >' + Usuario.apellido + '</td>';
            tr += '<td >' + Usuario.correo + '</td>';
            tr += '<td >' + Usuario.contrasena + '</td>';

            tr += '<td >' + Usuario.c_nombre_app + '</td>';
            tr += '<td >' + Usuario.c_nombre_empresa + '</td>';
            tr += '<td >' + Usuario.tipo + '</td>';
            tr += '<td >' + Usuario.fecha_creacion + '</td>';
            tr += '<td >' + Usuario.fecha_actualizacion + '</td>';
          
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-warning btn-editar-usuario ttip" data-idusuario="' + Usuario.id_usuario + '" data-placement="top" data-toggle="tooltip" title="Editar Usuario"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
            
            tr += '<td>';
                      tr += '<button class="btn btn-xs btn-danger btn-eliminar-usuario ttip" data-idusuario="' + Usuario.id_usuario + '" data-placement="top" data-toggle="tooltip" title="Borrar Usuario"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
            tr += '</td>';
            tr += '</tr>';
            $('#tb-usuario tbody').append(tr);
          });
          $('#tb-usuario tbody .ttip').tooltip();
          $('#pagination').append(resp.links);
          $('#cargando').modal('toggle');
          $('#row-usuarios').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });

      };
      ////////////////////////////////////////////////////////
      $('#btn-crear-usuario').click(function(){
         $('#crear-usuario').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-usuario').click(function(){
        var app=$('select[name="app"]').val();
        var empresa=$('select[name="empresa"]').val();
        var sucursal=$('select[name="sucursal"]').val();

        if( $('#crear-usuario .nomusu').val() == ''){
          toastr.error('DEBE INGRESAR EL NOMBRE DEL USUARIO');
          return false;
        }
         if( $('#crear-usuario .apeusu').val() == ''){
          toastr.error('DEBE INGRESAR EL APELLIDO DEL USUARIO');
          return false;
        }
         if( $('#crear-usuario .correo').val() == ''){
          toastr.error('DEBE INGRESAR EL CORREO');
          return false;
        }
         if( $('#crear-usuario .password').val() == ''){
          toastr.error('DEBE INGRESAR LA CONTRASEÑA');
          return false;
        }
         if( $('#crear-usuario .tel').val() == ''){
          toastr.error('DEBE INGRESAR EL TELEFONO');
          return false;
        }
        if( app == ''){
          toastr.error('DEBE SELECCIONAR APP');
          return false;
        }
        

          if( empresa == ''){
          toastr.error('DEBE SELECCIONAR EMPRESA');
          return false;
        }
    
        var data = {};
        data['nomusu'] = $('#crear-usuario .nomusu').val();
        data['apeusu'] = $('#crear-usuario .apeusu').val();  
        data['correo'] = $('#crear-usuario .correo').val(); 
        data['password'] = $('#crear-usuario .password').val();
        data['tel'] = $('#crear-usuario .tel').val();
        data['app'] = app;
        data['sucursal'] = sucursal;
        data['empresa'] = empresa;
        $('#crear-usuario').modal('toggle');
        $('#cargando').modal();
        $.post('Agregar_Usuario.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            $('#crear-usuario').modal('');
            return false;
          }else{
            $('#cargando').modal('toggle');
            var app=$('select[name="app"]').val();
            var sucursal=$('select[name="sucursal"]').val();
            var empresa=$('select[name="empresa"]').val();
            $('#crear-usuario .nomusu').val('');
            $('#crear-usuario .apeusu').val('');
            $('#crear-usuario .correo').val('');
            $('#crear-usuario .password').val('');
            $('#crear-usuario .tel').val('');
            toastr.success('USUARIO AGREGADO CORRECTAMENTE');
            listarUsuarios(1);
          }
        },'json').fail(function(){
         listarUsuarios(1);
         toastr.success('USUARIO AGREGADO CORRECTAMENTE');
        });
      });
       /////////////////////////////////////////////////////////////////////

      $(document).on('click','.btn-editar-usuario',function(){
        var id = $(this).data('idusuario');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Usuario.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-usuario .idusuario').val(resp.data.id_usuario);
            $('#f-editar-usuario .nomusu').val(resp.data.nombre);
            $('#f-editar-usuario .apeusu').val(resp.data.apellido);
            $('#f-editar-usuario .correo').val(resp.data.correo);
            $('#f-editar-usuario .password').val(resp.data.contrasena);
            $('#f-editar-usuario .tel').val(resp.data.telefono);
            $('#f-editar-usuario .eapp').val(resp.data.id_app);
           $('#f-editar-usuario .eempresa').val(resp.data.id_empresa);
            $('#f-editar-usuario .esucursal').val(resp.data.id_sucursal);
            $('#editar-usuario').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-usuario').click(function(){
         var app=$('select[name="eapp"]').val();
         var empresa=$('select[name="eempresa"]').val();
        var sucursal=$('select[name="esucursal"]').val();
        if( $('#f-editar-usuario .nomusu').val() == '0'){
          toastr.error('DEBE INGRESAR NOMBRE DEL USUARIO');
          return false;
        }
        if( $('#f-editar-usuario .apeusu').val() == '0'){
          toastr.error('DEBE INGRESAR APELLIDO DEL USUARIO');
          return false;
        }
         if( $('#f-editar-usuario .correo').val() == '0'){
          toastr.error('DEBE INGRESAR CORREO');
          return false;
        }
         if( $('#f-editar-usuario .password').val() == '0'){
          toastr.error('DEBE INGRESAR CONTRASEÑA');
          return false;
        }
         if( $('#f-editar-usuario .tel').val() == '0'){
          toastr.error('DEBE INGRESAR TELEFONO');
          return false;
        }
         if( app == '0'){
          toastr.error('DEBE SELECCIONAR APP');
          return false;
        }
        if( sucursal == '0'){
          toastr.error('DEBE SELECCIONAR SUCURSAL');
          return false;
        }
        if( empresa == '0'){
          toastr.error('DEBE SELECCIONAR EMPRESA');
          return false;
        }
        var data = {};
        data['idusuario'] = $('#f-editar-usuario .idusuario').val();
        data['nomusu'] = $('#f-editar-usuario .nomusu').val();
        data['apeusu'] = $('#f-editar-usuario .apeusu').val();
        data['correo'] = $('#f-editar-usuario .correo').val();
        data['password'] = $('#f-editar-usuario .password').val();
        data['tel'] = $('#f-editar-usuario .tel').val();
        data['app'] = app;
        data['sucursal'] = sucursal;
        data['empresa'] = empresa;
        $.post('Actualizar_Usuario.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-usuario').modal('toggle');
            toastr.success('USUARIO ACTUALIZADO CORRECTAMENTE');
            listarUsuarios(1);
          }
        },'json').fail(function(){
         $('#editar-usuario').modal('toggle');
           toastr.success('USUARIO ACTUALIZADO CORRECTAMENTE');
           listarUsuarios(1);
        });
      });
       ///////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-usuario',function(){
        var id = $(this).data('idusuario');
        swal({
            title: "",
            text: "¿Esta seguro que desea eliminar el usuario?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) {
              var data = {};
              data['id'] = id;
              $.post('Eliminar_Usuario.php',data,function(resp){
                if(resp.error){
                  toastr.error('ERROR: ' + resp.message);
                }else{
                  toastr.success('USUARIO ELIMINADO CORRECTAMENTE');
                  listarUsuarios(1);
                }

              },'json').fail(function(){
                toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
              });

            }
        });

      });
     
      /////////////////////////////////////////////////////////

      //////////////////////////////////////////////////////////////////////////////
  });
</script>
</body>
</html>
