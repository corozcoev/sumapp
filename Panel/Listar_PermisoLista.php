<?php

require_once "Controllers/conexion.php";

$page= $_GET['page'];

$query = "SELECT PL.id_permisolista,U.nombre,U.apellido,L.c_nombre_lista,PL.fecha_creacion,PL.fecha_actualizacion FROM tb_permiso_lista PL
INNER JOIN tb_usuario U ON U.id_usuario=PL.id_usuario
INNER JOIN tb_lista L ON L.id_lista=PL.id_lista
 		  order by PL.id_permisolista	
		  limit ".(10*($page-1)).",".(10*($page));
$data = array();
try {
	$resultado = mysqli_query($conexion,$query);
	while( $row = mysqli_fetch_assoc($resultado)){
	    $data[$row['id_permisolista']] = $row;
	}
	$resp['error']=false;	
} catch (Exception $e) {	
	$resp['error']=true;	
}

$resp['data']=$data;
echo json_encode($resp);

?>
