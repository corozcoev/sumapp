<?php include 'Encabezado.php' ?>
  <div class="content-wrapper">
    <div class="col-sm-12" style="background: #ecf0f5;">
      <section id="main-content">
      <section class="wrapper" style="background: none;">
        <br>
         <div id="row-piso" class="row">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Gestion Pisos :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                              <button type="button" class="btn btn-success" id="btn-ver-piso"><i class="fa fa-bars"></i> Ver Pisos<i class="preloader preloader-success hidden"></i></button>
                              <button type="button" class="btn btn-primary" id="btn-crear-piso"><i class="fa fa-plus-circle"></i> Nuevo Piso <i class="preloader preloader-info hidden"></i></button>
                            </div>
                         </div>
                    </div>
                  </fieldset>
                </form>
                <div class="row" id="row-pisos" style="display: none;">
                  <div class="col-md-12">
                    <section style="border: 1px solid #e0e0e2;" class="panel">
                      <table class="table table-striped table-hover" id="tb-piso">
                        <thead>
                          <tr>
                            <th>Numero de piso</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
                <div class="row" id="pagination">
                </div>
              </div>
            </section>
          </div>
        </div>
    


      </section>
    </section>
    </div>
  </div>
</div>

<div class="modal fade" id="crear-piso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-piso">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nuevo Piso</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Numero de piso</label>
                <input class="form-control numpiso" name="numpiso" type="number" required >
                <br>
  
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-piso">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editar-piso" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-piso">
          <input type="hidden" class="idpiso">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Piso</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Numero de piso</label>
                  <input class="form-control numpiso" name="numpiso" type="number" required >
                                  
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-piso">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>





<div class="modal" id="cargando" data-backdrop="static" style="top:40%">
  <div class="modal-dialog" style="width: 155px;">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="loader.gif"></center>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
<script src="Views/bower_components/fuelux/spinner.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- DESDE AQUI COMIENZA EL CODIGO JS QUE SE EJECUTA CUANDO LE DAS CLICKS A LOS BOTONES. ES JQUERY BASICO -->
<script>
  $(function(){
      $('#row-pisos').hide();
   
      $('#row-atributo').hide();


    ////////////////////////////////////////////////////////
      $('#btn-ver-piso').click(function(){
         listarPisos(1);
      });
    
      /////////////////////////////////////////////////////////
      function listarPisos(page){
        $('#row-atributo').hide();
        $('#cargando').modal();
        var data={};
        data['page']=page;
        //SE OBTIENEN LAS ENCUESTAS HACIENDO LLAMADA AL ARCHIVO LISTAR_ENCUESTA.PHP. LA RESPUESTA ES LA VARIABLE RESP
        $.get('Listar_Piso.php',data,function(resp){
          $('#tb-piso tbody').empty();
          $('#pagination').empty();
          var i=0;
          $.each(resp.data,function(key,Piso){
            var tr = '';
            i++;
            tr += '<tr id="piso-'+ Piso.id_piso +'">';
            tr += '<td >' + Piso.piso + '</td>';
            tr += '<td >' + Piso.fecha_creacion + '</td>';
            tr += '<td >' + Piso.fecha_actualizacion + '</td>';
          
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-warning btn-editar-piso ttip" data-idpiso="' + Piso.id_piso + '" data-placement="top" data-toggle="tooltip" title="Editar Piso"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
            if(Piso.c_tipo_piso=='D'){
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-primary btn-gestionar-piso ttip" data-idpiso="' + Piso.id_piso + '" data-nomlista="'+Piso.piso+'" data-placement="top" data-toggle="tooltip" title=""><i class="fa fa-user fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';

            }
            tr += '<td>';
                      tr += '<button class="btn btn-xs btn-danger btn-eliminar-piso ttip" data-idpiso="' + Piso.id_piso + '" data-placement="top" data-toggle="tooltip" title="Borrar Piso"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
            tr += '</td>';
            tr += '</tr>';
            $('#tb-piso tbody').append(tr);
          });
          $('#tb-piso tbody .ttip').tooltip();
          $('#pagination').append(resp.links);
          $('#cargando').modal('toggle');
          $('#row-pisos').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });

      };
      ////////////////////////////////////////////////////////
      $('#btn-crear-piso').click(function(){
         $('#crear-piso').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-piso').click(function(){
        if( $('#crear-piso .numpiso').val() == ''){
          toastr.error('DEBE INGRESAR EL NUMERO DE PISO');
          return false;
        }
    
        var data = {};
        data['numpiso'] = $('#crear-piso .numpiso').val();
           
        $('#crear-piso').modal('toggle');
        $('#cargando').modal();
        $.post('Agregar_Piso.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            $('#crear-piso').modal('');
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#crear-piso .numpiso').val('');
            toastr.success('PISO AGREGADO CORRECTAMENTE');
            listarPisos(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       /////////////////////////////////////////////////////////////////////

      $(document).on('click','.btn-editar-piso',function(){
        var id = $(this).data('idpiso');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Piso.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-piso .idpiso').val(resp.data.id_piso);
            $('#f-editar-piso .numpiso').val(resp.data.piso);
            $('#f-editar-piso #tipopiso').val(resp.data.c_tipo_piso);
            $('#editar-piso').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-piso').click(function(){
        if( $('#f-editar-piso .numpiso').val() == '0'){
          toastr.error('DEBE INGRESAR NUMERO DE PISO');
          return false;
        }
        if( $('#f-editar-piso #tipopiso').val() == '0'){
          toastr.error('DEBE INGRESAR UN TIPO');
          return false;
        }
        var data = {};
        data['idpiso'] = $('#f-editar-piso .idpiso').val();
        data['numpiso'] = $('#f-editar-piso .numpiso').val();
       
        $.post('Actualizar_Piso.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-piso').modal('toggle');
            toastr.success('PISO ACTUALIZADO CORRECTAMENTE');
            listarPisos(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ///////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-piso',function(){
        var id = $(this).data('idpiso');
        swal({
            title: "",
            text: "¿Esta seguro que desea eliminar el piso?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) {
              var data = {};
              data['id'] = id;
              $.post('Eliminar_Piso.php',data,function(resp){
                if(resp.error){
                  toastr.error('ERROR: ' + resp.message);
                }else{
                  toastr.success('PISO ELIMINADO CORRECTAMENTE');
                  listarPisos(1);
                }

              },'json').fail(function(){
                toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
              });

            }
        });

      });
     
      /////////////////////////////////////////////////////////

      //////////////////////////////////////////////////////////////////////////////
  });
</script>
</body>
</html>
