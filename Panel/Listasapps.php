<?php include 'Encabezado.php' ?>
  <div class="content-wrapper">
    <div class="col-sm-12" style="background: #ecf0f5;">
      <section id="main-content">
      <section class="wrapper" style="background: none;">
        <br>
         <div id="row-lista" class="row">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Gestion Listas :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                              <button type="button" class="btn btn-success" id="btn-ver-lista"><i class="fa fa-bars"></i> Ver Listas <i class="preloader preloader-success hidden"></i></button>
                              <button type="button" class="btn btn-primary" id="btn-crear-lista"><i class="fa fa-plus-circle"></i> Nueva Lista <i class="preloader preloader-info hidden"></i></button>
                            </div>
                         </div>
                    </div>
                  </fieldset>
                </form>
                <div class="row" id="row-listas" style="display: none;">
                  <div class="col-md-12">
                    <section style="border: 1px solid #e0e0e2;" class="panel">
                      <table class="table table-striped table-hover" id="tb-lista">
                        <thead>
                          <tr>
                            <th>Nombre de lista</th>
                            <th>App</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Atributos</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
                <div class="row" id="pagination">
                </div>
              </div>
            </section>
          </div>
        </div>
        <div class="row" id="row-atributo" style="display: none;">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Atributos :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
            <div class="panel-body">
              <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                      <div class="row">
                          <div class="col-sm-4">
                              <div class="form-group">
                                <label for="">Lista:</label>
                                <input type="hidden" class="id_lista" value="">
                                <input type="text" class="form-control nombre_lista" disabled value="">
                              </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group">
                                 <div class="form-group">
                                     <label class="d-block">&nbsp;</label>
                                     <button type="button" class="btn btn-primary" id="btn-crear-atributo"><i class="fa fa-plus-circle"></i> Nuevo Atributo <i class="preloader preloader-info hidden"></i></button>
                                 </div>
                               </div>
                            </div>
                        </div>
                    </fieldset>
               </form>
               <section style="border: 1px solid #e0e0e2;" class="panel">
                <table class="table table-striped table-advance table-hover" id="tb-atributo">
                  <thead>
                    <tr>
                            <th>Atributo</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                 
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </section>
              </div>
            </section>
          </div>
        </div>


      </section>
    </section>
    </div>
  </div>
</div>

<div class="modal fade" id="crear-lista" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-lista">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nueva Lista</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Nombre de lista</label>
                <input class="form-control nomlista" name="nomlista" type="text" required >
                <br>
  
              </div>
              </div>
                 <?php 
$sqlapp="SELECT * FROM tb_app ";
$queryapp = $conexion->query($sqlapp);//Se ejecuta consulta
$arrayapp= array(); // Array donde vamos a guardar los datos 
while($resultadoapp = $queryapp->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayapp[]=$resultadoapp; // Guardar los resultados en la variable

}
 ?>
  <div class="form-group">
              <div class="col-sm-12">
                   <label for="fono6">App </label>
                <select name="app" class="form-control app">  
                 <option selected disabled>Selecciona app</option>
                 <?php foreach ($arrayapp as $a):?>
                 <option value="<?php echo $a->id_app ?>"><?php echo $a->c_nombre_app ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-lista">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="crear-atributo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-atributo">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nuevo Atributo</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Atributo</label>
                <input class="form-control nomatributo" name="nomatributo" type="text" required >
                <input type="hidden" class="idlista" value="">
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-atributo">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editar-lista" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-lista">
          <input type="hidden" class="idlista">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Lista</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre de lista</label>
                  <input class="form-control nomlista" name="nomlista" type="text" required >
                                  
                </div>
              </div>
                <?php 
$sqlapp="SELECT * FROM tb_app ";
$queryapp = $conexion->query($sqlapp);//Se ejecuta consulta
$arrayapp= array(); // Array donde vamos a guardar los datos 
while($resultadoapp = $queryapp->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayapp[]=$resultadoapp; // Guardar los resultados en la variable

}
 ?>
  <div class="form-group">
              <div class="col-sm-12">
                   <label for="fono5">App </label>
                <select name="eapp" class="form-control eapp">  
                 <option selected disabled>Selecciona app</option>
                 <?php foreach ($arrayapp as $a):?>
                 <option value="<?php echo $a->id_app ?>"><?php echo $a->c_nombre_app ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-lista">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="editar-atributo" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-atributo">
          <input type="hidden" class="idatributo">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Atributo</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Lista</label>
                  <input class="form-control nomlista" name="nomlista" type="text" disabled>
                </div>
                <div class="col-sm-12">
                    <label for="fono1">Atributo</label>
                  <input class="form-control nomatributo" name="nomatributo" type="text" required >
                </div>
               
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-atributo">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>




<div class="modal" id="cargando" data-backdrop="static" style="top:40%">
  <div class="modal-dialog" style="width: 155px;">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="loader.gif"></center>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
<script src="Views/bower_components/fuelux/spinner.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- DESDE AQUI COMIENZA EL CODIGO JS QUE SE EJECUTA CUANDO LE DAS CLICKS A LOS BOTONES. ES JQUERY BASICO -->
<script>
  $(function(){
      $('#row-listas').hide();
   
      $('#row-atributo').hide();


    ////////////////////////////////////////////////////////
      $('#btn-ver-lista').click(function(){
         listarListas(1);
      });
    
      /////////////////////////////////////////////////////////
      function listarListas(page){
        $('#row-atributo').hide();
        $('#cargando').modal();
        var data={};
        data['page']=page;
        //SE OBTIENEN LAS ENCUESTAS HACIENDO LLAMADA AL ARCHIVO LISTAR_ENCUESTA.PHP. LA RESPUESTA ES LA VARIABLE RESP
        $.get('Listar_Lista.php',data,function(resp){
          $('#tb-lista tbody').empty();
          $('#pagination').empty();
          var i=0;
          $.each(resp.data,function(key,Lista){
            var tr = '';
            i++;
            tr += '<tr id="lista-'+ Lista.id_lista +'">';
            tr += '<td width="15%">' + Lista.c_nombre_lista + '</td>';
            tr += '<td width="15%">' + Lista.c_nombre_app + '</td>';
            tr += '<td width="15%">' + Lista.fecha_creacion + '</td>';
            tr += '<td width="15%">' + Lista.fecha_actualizacion + '</td>';
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-info btn-atributos-lista ttip" data-idlista="' + Lista.id_lista + '" data-placement="top" data-toggle="tooltip" title="Gestionar atributos"><i class="fa fa-align-justify"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-warning btn-editar-lista ttip" data-idlista="' + Lista.id_lista + '" data-placement="top" data-toggle="tooltip" title="Editar Lista"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
            if(Lista.c_tipo_lista=='D'){
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-primary btn-gestionar-lista ttip" data-idlista="' + Lista.id_lista + '" data-nomlista="'+Lista.c_nombre_lista+'" data-placement="top" data-toggle="tooltip" title=""><i class="fa fa-user fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';

            }
            tr += '<td>';
                      tr += '<button class="btn btn-xs btn-danger btn-eliminar-lista ttip" data-idlista="' + Lista.id_lista + '" data-placement="top" data-toggle="tooltip" title="Borrar Lista"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
            tr += '</td>';
            tr += '</tr>';
            $('#tb-lista tbody').append(tr);
          });
          $('#tb-lista tbody .ttip').tooltip();
          $('#pagination').append(resp.links);
          $('#cargando').modal('toggle');
          $('#row-listas').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });

      };
      ////////////////////////////////////////////////////////
      $('#btn-crear-lista').click(function(){
         $('#crear-lista').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-lista').click(function(){
        var app=$('select[name="app"]').val();
        if( $('#crear-lista .nomlista').val() == ''){
          toastr.error('DEBE INGRESAR EL NOMBRE');
          return false;
        }
           if( app == ''){
          toastr.error('DEBE SELECCIONAR UN APP');
          return false;
        }
        var data = {};
        data['nomlista'] = $('#crear-lista .nomlista').val();
        data['app'] = app;   
        
        $('#crear-lista').modal('toggle');
        $('#cargando').modal();
        $.post('Agregar_Lista.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            $('#crear-lista').modal('');
            return false;
          }else{
            $('#cargando').modal('toggle');
            var app=$('select[name="app"]').val();
            $('#crear-lista .nomlista').val('');
            toastr.success('LISTA AGREGADA CORRECTAMENTE');
            listarListas(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ////////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-atributo',function(){
        var id = $(this).data('idatributo');
        var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Eliminar_Atributo.php',data,function(resp){
          $('#cargando').modal('toggle');
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#cargando').modal('toggle');
            toastr.success('ATRIBUTO ELIMINADO CORRECTAMENTE');
            listarAtributos($('#row-atributo .id_lista').val());
          }
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       
      ////////////////////////////////////////////////////////////////////////////

      $(document).on('click','.btn-editar-lista',function(){
        var id = $(this).data('idlista');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Lista.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-lista .idlista').val(resp.data.id_lista);
            $('#f-editar-lista .nomlista').val(resp.data.c_nombre_lista);
            $('#f-editar-lista .eapp').val(resp.data.app);
            $('#f-editar-lista #tipoapp').val(resp.data.c_tipo_lista);
            $('#editar-lista').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-lista').click(function(){
        var app=$('select[name="eapp"]').val();
        if( $('#f-editar-lista .nomlista').val() == '0'){
          toastr.error('DEBE INGRESAR UN NOMBRE');
          return false;
        }
        if( $('#f-editar-lista #tipolista').val() == '0'){
          toastr.error('DEBE INGRESAR UN TIPO');
          return false;
        }
           if( app == '0'){
          toastr.error('DEBE SELECCIONAR APP');
          return false;
        }
        var data = {};
        data['idlista'] = $('#f-editar-lista .idlista').val();
        data['nomlista'] = $('#f-editar-lista .nomlista').val();
        data['app'] = app;
        
        $.post('Actualizar_Lista.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-lista').modal('toggle');
            toastr.success('LISTA ACTUALIZADA CORRECTAMENTE');
            listarListas(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ///////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-lista',function(){
        var id = $(this).data('idlista');
        swal({
            title: "",
            text: "¿Esta seguro que desea eliminar la lista?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) {
              var data = {};
              data['id'] = id;
              $.post('Eliminar_Lista.php',data,function(resp){
                if(resp.error){
                  toastr.error('ERROR: ' + resp.message);
                }else{
                  toastr.success('LISTA ELIMINADA CORRECTAMENTE');
                  listarListas(1);
                }

              },'json').fail(function(){
                toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
              });

            }
        });

      });
       ////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-atributos-lista',function(){
        var id = $(this).data('idlista');
        if( id == ''){
          toastr.error('DEBE SELECCIONAR EL TIPO');
          return false;
        }

        listarAtributos(id);
      });
      /////////////////////////////////////////////////////////
      function listarAtributos(id){
             var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Listar_Atributo.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#row-atributo .nombre_lista').val(resp.lista.c_nombre_lista);
            $('#row-atributo .id_lista').val(resp.lista.id_lista);
            $('#tb-atributo tbody').empty();
          
            $.each(resp.data,function(key,Atributo){
             var tr = '';
              tr += '<tr data-idatributo="'+ Atributo.id_encuesta +'">';

             
              tr += '<td width="30%">' + Atributo.c_nombre_atributo + '</td>';
              
              tr += '<td>' + Atributo.fecha_creacion + '</td>';
              tr += '<td>' + Atributo.fecha_actualizacion + '</td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-warning btn-editar-atributo ttip" data-idatributo="' + Atributo.id_atributo + '" data-placement="top" data-toggle="tooltip" title="Editar Atributo"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-danger btn-eliminar-atributo ttip" data-idatributo="' + Atributo.id_atributo + '" data-placement="top" data-toggle="tooltip" title="Borrar Atributo"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '</tr>';
              $('#tb-atributo tbody').append(tr);
            });
          }
          $('#tb-atributo tbody .ttip').tooltip();
          $('#row-atributo').show();
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      }
       //////////////////////////////////////////////////////////////////////
 
      $('#btn-crear-atributo').click(function(){
        var idlista=$('#row-atributo .id_lista').val();
        var nomlista=$('#row-atributo .nombre_lista').val();
        $('#f-crear-atributo .idlista').val(idlista);
        $('#f-crear-atributo .nomalista').val(nomlista);
        $('#crear-atributo').modal();
      });
      //////////////////////////////////////////////////
           $('#sbmt-crear-atributo').click(function(){

        if( $('#crear-atributo .nomatributo').val() == '' ){
          toastr.error('DEBE INGRESAR EL ATRIBUTO');
          return false;
        }
        var data = {};
        data['idlista'] = $('#crear-atributo .idlista').val();
        data['nomatributo'] = $('#crear-atributo .nomatributo').val();
        console.log(data['idlista']);
         console.log(data['nomatributo']);
        $('#crear-atributo').modal('toggle');

        $.post('Agregar_Atributo.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
              $('#crear-atributo').modal('');
          }else{
            $('#crear-atributo .nomatributo').val('');
            listarAtributos( $('#crear-atributo .idlista').val())
            toastr.success('ATRIBUTO AGREGADO CORRECTAMENTE');
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////
      $(document).on('click','.btn-editar-atributo',function(){
        var id = $(this).data('idatributo');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Atributo.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-atributo .nomlista').val($('#row-atributo .nombre_lista').val());
            $('#f-editar-atributo .idatributo').val(resp.data.id_atributo);
            $('#f-editar-atributo .nomatributo').val(resp.data.c_nombre_atributo);
            $('#editar-atributo').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //
      ////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-atributo').click(function(){
        if( $('#editar-atributo .nomatributo').val() == '' ){
          toastr.error('DEBE INGRESAR EL ATRIBUTO');
          return false;
        }
        var data = {};
        data['idatributo'] = $('#editar-atributo .idatributo').val();
        data['nomatributo'] = $('#editar-atributo .nomatributo').val();

        $('#editar-atributo').modal('toggle');

        $.post('Actualizar_Atributo.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
              $('#editar-atributo').modal('');
          }else{
            listarAtributos( $('#row-atributo .id_lista').val())
            toastr.success('ATRIBUTO ACTUALIZADO CORRECTAMENTE');
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
  
      
      //////////////////////////////////////////////////
      
     
   
      //////////////////////////////////////////////////////////////////////////////
  });
</script>
</body>
</html>
