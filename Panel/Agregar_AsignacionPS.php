<?php
//
require 'phpqrcode/qrlib.php';
require_once "Controllers/conexion.php";
date_default_timezone_set("America/Mexico_City");

$fecha= date("Y-m-d");
$hora=date('h:i A');
$app= $_POST['app'];
$cuestionario= $_POST['cuestionario'];
$bloque= $_POST['bloque'];
$sucursal= $_POST['sucursal'];
$piso= $_POST['piso'];
$sala= $_POST['sala'];
$dir = 'Qr/';
if(!file_exists($dir))
		mkdir($dir);
	
	$filename = $dir.''.$app.'-'.$cuestionario.'-'.$bloque.'.png';
	
	$tamanio = 15;
	$level = 'H';
	$frameSize = 1;
	$contenido ='https://app.sumapp.cloud/Usuario/resolver.php?bloque='.$bloque.'&sucursal='.$sucursal.'&piso='.$piso.'&sala='.$sala.'';

	QRcode::png($contenido, $filename, $level, $tamanio, $frameSize);

$query = "insert into tb_asignacion_ps (id_app,id_cuestionario,id_bloque,id_sucursal,id_piso,id_sala,qr,fecha_creacion,fecha_actualizacion) values ('".$app."','".$cuestionario."','".$bloque."','".$sucursal."','".$piso."','".$sala."','".$filename."','".$fecha."-".$hora."','".$fecha."-".$hora."')";

try {
	$resultado= mysqli_query($conexion,$query);	
	$resp['error']=false;
} catch (Exception $e) {
	$resp['error']=true;
}

echo json_encode($resp);

?>
