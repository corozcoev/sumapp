<?php include 'Encabezado.php' ?>
  <div class="content-wrapper">
    <div class="col-sm-12" style="background: #ecf0f5;">
      <section id="main-content">
      <section class="wrapper" style="background: none;">
        <br>
         <div id="row-empresa" class="row">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Gestion Empresas :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                              <button type="button" class="btn btn-success" id="btn-ver-empresa"><i class="fa fa-bars"></i> Ver Empresas <i class="preloader preloader-success hidden"></i></button>
                              <button type="button" class="btn btn-primary" id="btn-crear-empresa"><i class="fa fa-plus-circle"></i> Nueva Empresa <i class="preloader preloader-info hidden"></i></button>
                            </div>
                         </div>
                    </div>
                  </fieldset>
                </form>
                <div class="row" id="row-empresas" style="display: none;">
                  <div class="col-md-12">
                    <section style="border: 1px solid #e0e0e2;" class="panel">
                      <table class="table table-striped table-hover" id="tb-empresa">
                        <thead>
                          <tr>
                            <th>Nombre de empresa</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Sucursales</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
                <div class="row" id="pagination">
                </div>
              </div>
            </section>
          </div>
        </div>
        <div class="row" id="row-sucursal" style="display: none;">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Sucursales :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
            <div class="panel-body">
              <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                      <div class="row">
                          <div class="col-sm-4">
                              <div class="form-group">
                                <label for="">Empresa:</label>
                                <input type="hidden" class="id_empresa" value="">
                                <input type="text" class="form-control nombre_empresa" disabled value="">
                              </div>
                           </div>
                           <div class="col-sm-4">
                               <div class="form-group">
                                 <div class="form-group">
                                     <label class="d-block">&nbsp;</label>
                                     <button type="button" class="btn btn-primary" id="btn-crear-sucursal"><i class="fa fa-plus-circle"></i> Nueva sucursal <i class="preloader preloader-info hidden"></i></button>
                                 </div>
                               </div>
                            </div>
                        </div>
                    </fieldset>
               </form>
               <section style="border: 1px solid #e0e0e2;" class="panel">
                <table class="table table-striped table-advance table-hover" id="tb-sucursal">
                  <thead>
                    <tr>
                            <th>Sucursal</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                 
                  </thead>
                  <tbody>

                  </tbody>
                </table>
              </section>
              </div>
            </section>
          </div>
        </div>


      </section>
    </section>
    </div>
  </div>
</div>

<div class="modal fade" id="crear-empresa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-empresa">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nueva Empresa</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Nombre de empresa</label>
                <input class="form-control nomempresa" name="nomempresa" type="text" required >
                <br>
  
              </div>
              </div>
 
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-empresa">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="crear-sucursal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-sucursal">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nueva Sucursal</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Nombre de sucursal</label>
                <input class="form-control nomsucursal" name="nomsucursal" type="text" required >
                <input type="hidden" class="idempresa" value="">
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-sucursal">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editar-empresa" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-empresa">
          <input type="hidden" class="idempresa">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Empresa</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Nombre de empresa</label>
                  <input class="form-control nomempresa" name="nomempresa" type="text" required >
                                  
                </div>
              </div>
     
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-empresa">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>
<div class="modal fade" id="editar-sucursal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-sucursal">
          <input type="hidden" class="idsucursal">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Sucursal</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Empresa</label>
                  <input class="form-control nomempresa" name="nomempresa" type="text" disabled>
                </div>
                <div class="col-sm-12">
                    <label for="fono1">Nombre de sucursal</label>
                  <input class="form-control nomsucursal" name="nomsucursal" type="text" required >
                </div>
               
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-sucursal">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>




<div class="modal" id="cargando" data-backdrop="static" style="top:40%">
  <div class="modal-dialog" style="width: 155px;">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="loader.gif"></center>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
<script src="Views/bower_components/fuelux/spinner.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- DESDE AQUI COMIENZA EL CODIGO JS QUE SE EJECUTA CUANDO LE DAS CLICKS A LOS BOTONES. ES JQUERY BASICO -->
<script>
  $(function(){
      $('#row-empresas').hide();
   
      $('#row-sucursal').hide();


    ////////////////////////////////////////////////////////
      $('#btn-ver-empresa').click(function(){
         listarEmpresas(1);
      });
    
      /////////////////////////////////////////////////////////
      function listarEmpresas(page){
        $('#row-sucursal').hide();
        $('#cargando').modal();
        var data={};
        data['page']=page;
        //SE OBTIENEN LAS ENCUESTAS HACIENDO LLAMADA AL ARCHIVO LISTAR_ENCUESTA.PHP. LA RESPUESTA ES LA VARIABLE RESP
        $.get('Listar_Empresa.php',data,function(resp){
          $('#tb-empresa tbody').empty();
          $('#pagination').empty();
          var i=0;
          $.each(resp.data,function(key,Empresa){
            var tr = '';
            i++;
            tr += '<tr id="empresa-'+ Empresa.id_empresa +'">';
            tr += '<td width="60%">' + Empresa.c_nombre_empresa + '</td>';
            tr += '<td width="15%">' + Empresa.fecha_creacion + '</td>';
            tr += '<td width="15%">' + Empresa.fecha_actualizacion + '</td>';
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-info btn-sucursales-empresa ttip" data-idempresa="' + Empresa.id_empresa + '" data-placement="top" data-toggle="tooltip" title="Gestionar sucursales"><i class="fa fa-university"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-warning btn-editar-empresa ttip" data-idempresa="' + Empresa.id_empresa + '" data-placement="top" data-toggle="tooltip" title="Editar Empresa"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
            if(Empresa.c_tipo_empresa=='D'){
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-primary btn-gestionar-empresa ttip" data-idempresa="' + Empresa.id_empresa + '" data-nomempresa="'+Empresa.c_nombre_empresa+'" data-placement="top" data-toggle="tooltip" title=""><i class="fa fa-user fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';

            }
            tr += '<td>';
                      tr += '<button class="btn btn-xs btn-danger btn-eliminar-empresa ttip" data-idempresa="' + Empresa.id_empresa + '" data-placement="top" data-toggle="tooltip" title="Borrar Empresa"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
            tr += '</td>';
            tr += '</tr>';
            $('#tb-empresa tbody').append(tr);
          });
          $('#tb-empresa tbody .ttip').tooltip();
          $('#pagination').append(resp.links);
          $('#cargando').modal('toggle');
          $('#row-empresas').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });

      };
      ////////////////////////////////////////////////////////
      $('#btn-crear-empresa').click(function(){
         $('#crear-empresa').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-empresa').click(function(){
        if( $('#crear-empresa .nomempresa').val() == ''){
          toastr.error('DEBE INGRESAR EL NOMBRE');
          return false;
        }
         
        var data = {};
        data['nomempresa'] = $('#crear-empresa .nomempresa').val();
          
        
        $('#crear-empresa').modal('toggle');
        $('#cargando').modal();
        $.post('Agregar_Empresa.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            $('#crear-empresa').modal('');
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#crear-empresa .nomempresa').val('');
            toastr.success('EMPRESA AGREGADA CORRECTAMENTE');
            listarEmpresas(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ////////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-sucursal',function(){
        var id = $(this).data('idsucursal');
        var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Eliminar_Sucursal.php',data,function(resp){
          $('#cargando').modal('toggle');
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#cargando').modal('toggle');
            toastr.success('SUCURSAL ELIMINADA CORRECTAMENTE');
            listarSucursales($('#row-sucursal .id_empresa').val());
          }
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       
      ////////////////////////////////////////////////////////////////////////////

      $(document).on('click','.btn-editar-empresa',function(){
        var id = $(this).data('idempresa');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Empresa.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-empresa .idempresa').val(resp.data.id_empresa);
            $('#f-editar-empresa .nomempresa').val(resp.data.c_nombre_empresa);
            $('#f-editar-empresa #tipoempresa').val(resp.data.c_tipo_empresa);
            $('#editar-empresa').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-empresa').click(function(){
        if( $('#f-editar-empresa .nomempresa').val() == '0'){
          toastr.error('DEBE INGRESAR UN NOMBRE');
          return false;
        }
        if( $('#f-editar-lista #tipoempresa').val() == '0'){
          toastr.error('DEBE INGRESAR UN TIPO');
          return false;
        }
        
        var data = {};
        data['idempresa'] = $('#f-editar-empresa .idempresa').val();
        data['nomempresa'] = $('#f-editar-empresa .nomempresa').val();
        
        
        $.post('Actualizar_Empresa.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-empresa').modal('toggle');
            toastr.success('EMPRESA ACTUALIZADA CORRECTAMENTE');
            listarEmpresas(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ///////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-empresa',function(){
        var id = $(this).data('idempresa');
        swal({
            title: "",
            text: "¿Esta seguro que desea eliminar la empresa?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) {
              var data = {};
              data['id'] = id;
              $.post('Eliminar_Empresa.php',data,function(resp){
                if(resp.error){
                  toastr.error('ERROR: ' + resp.message);
                }else{
                  toastr.success('EMPRESA ELIMINADA CORRECTAMENTE');
                  listarEmpresas(1);
                }

              },'json').fail(function(){
                toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
              });

            }
        });

      });
       ////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-sucursales-empresa',function(){
        var id = $(this).data('idempresa');
        if( id == ''){
          toastr.error('DEBE SELECCIONAR EL TIPO');
          return false;
        }

        listarSucursales(id);
      });
      /////////////////////////////////////////////////////////
      function listarSucursales(id){
             var data = {};
        data['id'] = id;
        $('#cargando').modal();
        $.post('Listar_Sucursal.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#row-sucursal .nombre_empresa').val(resp.empresa.c_nombre_empresa);
            $('#row-sucursal .id_empresa').val(resp.empresa.id_empresa);
            $('#tb-sucursal tbody').empty();
          
            $.each(resp.data,function(key,Sucursal){
             var tr = '';
              tr += '<tr data-idsucursal="'+ Sucursal.id_sucursal +'">';

             
              tr += '<td width="30%">' + Sucursal.sucursal + '</td>';
              
              tr += '<td>' + Sucursal.fecha_creacion + '</td>';
              tr += '<td>' + Sucursal.fecha_actualizacion + '</td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-warning btn-editar-sucursal ttip" data-idsucursal="' + Sucursal.id_sucursal + '" data-placement="top" data-toggle="tooltip" title="Editar Sucursal"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-danger btn-eliminar-sucursal ttip" data-idsucursal="' + Sucursal.id_sucursal + '" data-placement="top" data-toggle="tooltip" title="Borrar Sucursal"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';
              tr += '</tr>';
              $('#tb-sucursal tbody').append(tr);
            });
          }
          $('#tb-sucursal tbody .ttip').tooltip();
          $('#row-sucursal').show();
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      }
       //////////////////////////////////////////////////////////////////////
 
      $('#btn-crear-sucursal').click(function(){
        var idempresa=$('#row-sucursal .id_empresa').val();
        var nomempresa=$('#row-sucursal .nombre_empresa').val();
        $('#f-crear-sucursal .idempresa').val(idempresa);
        $('#f-crear-sucursal .nomempresa').val(nomempresa);
        $('#crear-sucursal').modal();
      });
      //////////////////////////////////////////////////
           $('#sbmt-crear-sucursal').click(function(){

        if( $('#crear-sucursal .nomsucursal').val() == '' ){
          toastr.error('DEBE INGRESAR LA SUCURSAL');
          return false;
        }
        var data = {};
        data['idempresa'] = $('#crear-sucursal .idempresa').val();
        data['nomsucursal'] = $('#crear-sucursal .nomsucursal').val();
        console.log(data['idempresa']);
         console.log(data['nomsucursal']);
        $('#crear-sucursal').modal('toggle');

        $.post('Agregar_Sucursal.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
              $('#crear-sucursal').modal('');
          }else{
            $('#crear-sucursal .nomsucursal').val('');
            listarSucursales( $('#crear-sucursal .idempresa').val())
            toastr.success('SUCURSAL AGREGADA CORRECTAMENTE');
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////
      $(document).on('click','.btn-editar-sucursal',function(){
        var id = $(this).data('idsucursal');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Sucursal.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-sucursal .nomempresa').val($('#row-sucursal .nombre_empresa').val());
            $('#f-editar-sucursal .idsucursal').val(resp.data.id_sucursal);
            $('#f-editar-sucursal .nomsucursal').val(resp.data.sucursal);
            $('#editar-sucursal').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //
      ////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-sucursal').click(function(){
        if( $('#editar-sucursal .nomsucursal').val() == '' ){
          toastr.error('DEBE INGRESAR LA SUCURSAL');
          return false;
        }
        var data = {};
        data['idsucursal'] = $('#editar-sucursal .idsucursal').val();
        data['nomsucursal'] = $('#editar-sucursal .nomsucursal').val();

        $('#editar-sucursal').modal('toggle');

        $.post('Actualizar_Sucursal.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
              $('#editar-sucursal').modal('');
          }else{
            listarSucursales( $('#row-sucursal .id_empresa').val())
            toastr.success('SUCURSAL ACTUALIZADA CORRECTAMENTE');
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
  
      
      //////////////////////////////////////////////////
      
     
   
      //////////////////////////////////////////////////////////////////////////////
  });
</script>
</body>
</html>
