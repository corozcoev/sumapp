<?php include 'Encabezado.php' ?>
  <div class="content-wrapper">
    <div class="col-sm-12" style="background: #ecf0f5;">
      <section id="main-content">
      <section class="wrapper" style="background: none;">
        <br>
         <div id="row-asignacion" class="row">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Asignacion P/S :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                              <button type="button" class="btn btn-success" id="btn-ver-asignacion"><i class="fa fa-bars"></i> Ver Asignaciones<i class="preloader preloader-success hidden"></i></button>
                              <button type="button" class="btn btn-primary" id="btn-crear-asignacion"><i class="fa fa-plus-circle"></i> Nueva Asignacion <i class="preloader preloader-info hidden"></i></button>
                            </div>
                         </div>
                    </div>
                  </fieldset>
                </form>
                <div class="row" id="row-asignaciones" style="display: none;">
                  <div class="col-md-12">
                    <section style="border: 1px solid #e0e0e2;" class="panel">
                      <table class="table table-striped table-hover" id="tb-asignacion">
                        <thead>
                          <tr>
                            <th>App</th>
                            <th>Cuestionario</th>
                            <th>B.Preguntas</th>
                            <th>Sucursal</th>
                            <th>Piso</th>
                            <th>Sala</th>
                            <th>Qr</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <!--<th>Editar</th>-->
                            <th>Eliminar</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
                <div class="row" id="pagination">
                </div>
              </div>
            </section>
          </div>
        </div>
    


      </section>
    </section>
    </div>
  </div>
</div>

<div class="modal fade" id="crear-asignacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-asignacion">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nueva Asignacion</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                <?php 
$sqlapp="SELECT id_app,c_nombre_app FROM tb_app ";
$queryapp = $conexion->query($sqlapp);//Se ejecuta consulta
$arrayapp= array(); // Array donde vamos a guardar los datos 
while($resultadoapp = $queryapp->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arrayapp[]=$resultadoapp; // Guardar los resultados en la variable

}
 ?>



                  <label for="fono1">App </label>
                <select name="app" class="form-control app" onchange="Obtenervalorapp(this)">  
                 <option selected disabled>Selecciona app</option>
                 <?php foreach ($arrayapp as $a):?>
                 <option value="<?php echo $a->id_app ?>"><?php echo $a->c_nombre_app ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
                
              </div>
              </div>
               
               <script type="text/javascript">
   function Obtenervalorapp(selectObject) {
    var valor = selectObject.value;  

  $.ajax({
        data:{'valor':valor}, // Adjuntamos los parametros
        url:'Cuestionario_AsignacionPS.php', // ruta del archivo php que procesará nuestra solicitud
        type:  'get', // metodo por el cual se mandarán los datos
        beforeSend: function () { // callback que se ejecutará antes de enviar la solicitud
          console.log("Enviando por medio de post");
        },
        success:  function (response) { // callback que se ejecutará una vez el servidor responda
           $("#cuestionarios").html(response);
        }
      });
    }


 </script>
  <div class="form-group">
              <div class="col-sm-12" id="cuestionarios">
                
             
              </div>
              </div>

                <div class="form-group">
              <div class="col-sm-12" id="bloques">
                
             
              </div>
              </div>
                  <div class="form-group">
              <div class="col-sm-12">
                             <?php 
$sqlsucursal="SELECT id_sucursal,sucursal FROM tb_sucursal ";
$querysucursal = $conexion->query($sqlsucursal);//Se ejecuta consulta
$arraysucursal= array(); // Array donde vamos a guardar los datos 
while($resultadosucursal = $querysucursal->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraysucursal[]=$resultadosucursal; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Sucursal</label>
                 <select name="sucursal" class="form-control sucursal">  
                 <option selected disabled>Selecciona sucursal</option>
                 <?php foreach ($arraysucursal as $s):?>
                 <option value="<?php echo $s->id_sucursal ?>"><?php echo $s->sucursal ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>
                  <div class="form-group">
              <div class="col-sm-12">
                             <?php 
$sqlpiso="SELECT id_piso,piso FROM tb_piso ";
$querypiso = $conexion->query($sqlpiso);//Se ejecuta consulta
$arraypiso= array(); // Array donde vamos a guardar los datos 
while($resultadopiso = $querypiso->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraypiso[]=$resultadopiso; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Piso</label>
                 <select name="piso" class="form-control piso">  
                 <option selected disabled>Selecciona piso</option>
                 <?php foreach ($arraypiso as $p):?>
                 <option value="<?php echo $p->id_piso ?>"><?php echo $p->piso ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>
                   <div class="form-group">
              <div class="col-sm-12">
                             <?php 
$sqlsala="SELECT id_sala,sala FROM tb_sala ";
$querysala = $conexion->query($sqlsala);//Se ejecuta consulta
$arraysala= array(); // Array donde vamos a guardar los datos 
while($resultadosala = $querysala->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraysala[]=$resultadosala; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Sala</label>
                 <select name="sala" class="form-control sala">  
                 <option selected disabled>Selecciona sala</option>
                 <?php foreach ($arraysala as $s):?>
                 <option value="<?php echo $s->id_sala ?>"><?php echo $s->sala ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-asignacion">Asignar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editar-asignacion" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-asignacion">
          <input type="hidden" class="idasignacion">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Asignacion</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
               <div class="form-group">
              <div class="col-sm-12">
                <?php 
$sqlcuestionario="SELECT id_encuesta,c_nombre_encuesta FROM tb_encuesta ";
$querycuestionario = $conexion->query($sqlcuestionario);//Se ejecuta consulta
$arraycuestionario= array(); // Array donde vamos a guardar los datos 
while($resultadocuestionario = $querycuestionario->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraycuestionario[]=$resultadocuestionario; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Cuestionario</label>
                <select name="ecuestionario" class="form-control cuestionario">  
                 <option selected disabled>Selecciona cuestionario</option>
                 <?php foreach ($arraycuestionario as $c):?>
                 <option value="<?php echo $c->id_encuesta ?>"><?php echo $c->c_nombre_encuesta ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>

                  <div class="form-group">
              <div class="col-sm-12">
                             <?php 
$sqlpiso="SELECT id_piso,piso FROM tb_piso ";
$querypiso = $conexion->query($sqlpiso);//Se ejecuta consulta
$arraypiso= array(); // Array donde vamos a guardar los datos 
while($resultadopiso = $querypiso->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraypiso[]=$resultadopiso; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Piso</label>
                 <select name="episo" class="form-control piso">  
                 <option selected disabled>Selecciona piso</option>
                 <?php foreach ($arraypiso as $p):?>
                 <option value="<?php echo $p->id_piso ?>"><?php echo $p->piso ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>
                   <div class="form-group">
              <div class="col-sm-12">
                             <?php 
$sqlsala="SELECT id_sala,sala FROM tb_sala ";
$querysala = $conexion->query($sqlsala);//Se ejecuta consulta
$arraysala= array(); // Array donde vamos a guardar los datos 
while($resultadosala = $querysala->fetch_object()){ // Recorrer los resultados de Ejecutar la consulta SQL
    $arraysala[]=$resultadosala; // Guardar los resultados en la variable

}
 ?>
                  <label for="fono1">Sala</label>
                 <select name="esala" class="form-control sala">  
                 <option selected disabled>Selecciona sala</option>
                 <?php foreach ($arraysala as $s):?>
                 <option value="<?php echo $s->id_sala ?>"><?php echo $s->sala ?></option>
                 <?php endforeach; ?>
                </select>
                <br>
  
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-asignacion">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>





<div class="modal" id="cargando" data-backdrop="static" style="top:40%">
  <div class="modal-dialog" style="width: 155px;">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="loader.gif"></center>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
<script src="Views/bower_components/fuelux/spinner.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- DESDE AQUI COMIENZA EL CODIGO JS QUE SE EJECUTA CUANDO LE DAS CLICKS A LOS BOTONES. ES JQUERY BASICO -->
<script>
  $(function(){
      $('#row-asignaciones').hide();
   


    ////////////////////////////////////////////////////////
      $('#btn-ver-asignacion').click(function(){
         listarAsignaciones(1);
      });
    
      /////////////////////////////////////////////////////////
      function listarAsignaciones(page){
        $('#cargando').modal();
        var data={};
        data['page']=page;
        //SE OBTIENEN LAS ENCUESTAS HACIENDO LLAMADA AL ARCHIVO LISTAR_ENCUESTA.PHP. LA RESPUESTA ES LA VARIABLE RESP
        $.get('Listar_AsignacionPS.php',data,function(resp){
          $('#tb-asignacion tbody').empty();
          $('#pagination').empty();
          var i=0;
          $.each(resp.data,function(key,Asignacion){
            var tr = '';
            i++;
            tr += '<tr id="asignacion-'+ Asignacion.id_asignacionps +'">';
            tr += '<td >' + Asignacion.c_nombre_app + '</td>';
            tr += '<td >' + Asignacion.c_nombre_encuesta + '</td>';
            tr += '<td >' + Asignacion.c_nombre_bloque + '</td>';
            tr += '<td >' + Asignacion.sucursal + '</td>';
            tr += '<td >' + Asignacion.piso + '</td>';
            tr += '<td >' + Asignacion.sala + '</td>';
            tr += '<td ><img src="' + Asignacion.qr + '" width="50px" heigth="50px"/></td>';
            tr += '<td >' + Asignacion.fecha_creacion + '</td>';
            tr += '<td >' + Asignacion.fecha_actualizacion + '</td>';
          
          //  tr += '<td>';
                     // tr += '<center><button class="btn btn-xs btn-warning btn-editar-asignacion ttip" data-idasignacion="' + Asignacion.id_asignacionps + '" data-placement="top" data-toggle="tooltip" title="Editar Asignacion"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button></center>';
           // tr += '</td>';
            if(Asignacion.c_tipo_asignacion=='D'){
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-primary btn-gestionar-asignacion ttip" data-idasignacion="' + Asignacion.id_asignacionps + '" data-nomasignacion="'+Asignacion.id_cuestionario+'" data-placement="top" data-toggle="tooltip" title=""><i class="fa fa-user fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';

            }
            tr += '<td>';
                      tr += '<button class="btn btn-xs btn-danger btn-eliminar-asignacion ttip" data-idasignacion="' + Asignacion.id_asignacionps + '" data-placement="top" data-toggle="tooltip" title="Borrar Asignacion"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
            tr += '</td>';
            tr += '</tr>';
            $('#tb-asignacion tbody').append(tr);
          });
          $('#tb-asignacion tbody .ttip').tooltip();
          $('#pagination').append(resp.links);
          $('#cargando').modal('toggle');
          $('#row-asignaciones').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });

      };
      ////////////////////////////////////////////////////////
      $('#btn-crear-asignacion').click(function(){
         $('#crear-asignacion').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-asignacion').click(function(){
         var app=$('select[name="app"]').val();
        var cuestionario=$('select[name="cuestionario"]').val();
        var bloque=$('select[name="bloque"]').val();
        var sucursal=$('select[name="sucursal"]').val();
        var piso=$('select[name="piso"]').val();
        var sala=$('select[name="sala"]').val();

         if( app == ''){
          toastr.error('DEBE SELECCIONAR APP');
          return false;
        }

        if( cuestionario == ''){
          toastr.error('DEBE SELECCIONAR CUESTIONARIO');
          return false;
        }
        if( sucursal == ''){
          toastr.error('DEBE SELECCIONAR UNA SUCURSAL');
          return false;
        }
   if( bloque == ''){
          toastr.error('DEBE SELECCIONAR UN BLOQUE');
          return false;
        }
         if( piso == ''){
          toastr.error('DEBE SELECCIONAR PISO');
          return false;
        }


    
        var data = {};
        data['app'] = app;
        data['cuestionario'] = cuestionario;
        data['bloque'] = bloque;
        data['sucursal'] = sucursal;
        data['piso'] = piso;
        data['sala'] = sala;
           
        $('#crear-asignacion').modal('toggle');
        $('#cargando').modal();
        $.post('Agregar_AsignacionPS.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            $('#crear-asignacion').modal('');
            return false;
          }else{
            $('#cargando').modal('toggle');
        var app=$('select[name="app"]').val();
        var cuestionario=$('select[name="cuestionario"]').val();
        var bloque=$('select[name="bloque"]').val();
        var sucursal=$('select[name="sucursal"]').val();
        var piso=$('select[name="piso"]').val();
        var sala=$('select[name="sala"]').val();

            toastr.success('LA ASIGNACION SE HA REALIZADO CORRECTAMENTE');
            listarAsignaciones(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       /////////////////////////////////////////////////////////////////////

      $(document).on('click','.btn-editar-asignacion',function(){
        var id = $(this).data('idasignacion');
        var data = {};
        data['id'] = id;
        $.post('Obtener_AsignacionPS.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-asignacion .idasignacion').val(resp.data.id_asignacionps);
            $('#f-editar-asignacion .cuestionario').val(resp.data.id_cuestionario);
            $('#f-editar-asignacion .piso').val(resp.data.id_piso);
            $('#f-editar-asignacion .sala').val(resp.data.id_sala);
            $('#f-editar-asignacion #tipoasignacion').val(resp.data.c_tipo_asignacion);
            $('#editar-asignacion').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-asignacion').click(function(){
        var cuestionario=$('select[name="ecuestionario"]').val();
        var piso=$('select[name="episo"]').val();
        var sala=$('select[name="esala"]').val();

        if( cuestionario == '0'){
          toastr.error('DEBE SELECCIONAR UN CUESTIONARIO');
          return false;
        }
        if( piso == '0'){
          toastr.error('DEBE SELECCIONAR UN PISO');
          return false;
        }
        var data = {};
        data['idasignacion'] = $('#f-editar-asignacion .idasignacion').val();
        data['cuestionario'] = cuestionario;
        data['piso'] = piso;
        data['sala'] = sala;
       
        $.post('Actualizar_AsignacionPS.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-asignacion').modal('toggle');
            toastr.success('ASIGNACION ACTUALIZADA CORRECTAMENTE');
            listarAsignaciones(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ///////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-asignacion',function(){
        var id = $(this).data('idasignacion');
        console.log(id);
        swal({
            title: "",
            text: "¿Esta seguro que desea eliminar la asignacion?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) {
              var data = {};
              data['id'] = id;
              $.post('Eliminar_AsignacionPS.php',data,function(resp){
                if(resp.error){
                  toastr.error('ERROR: ' + resp.message);
                }else{
                  toastr.success('ASIGNACION ELIMINADA CORRECTAMENTE');
                  listarAsignaciones(1);
                }

              },'json').fail(function(){
                toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
              });

            }
        });

      });
     
      /////////////////////////////////////////////////////////

      //////////////////////////////////////////////////////////////////////////////
  });
</script>
</body>
</html>
