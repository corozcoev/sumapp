<?php
//Archivo para actualizar los bloques de preguntas. A la función mysqli_query se le pasan 2 parámetros:
//la conexion a la BD (que viene del archivo conexion.php en la línea 4, require_once hace que las variables que se crean en ese archivo sirvan en este) y la query. En este caso es un update, se le pasa el nombre de la tabla y las variables que se desean cambiar.
//$_POST envía los valores de las variables con ese name en el HTML
require_once "Controllers/conexion.php";
date_default_timezone_set("America/Mexico_City");

$fecha= date("Y-m-d");
$hora=date('h:i A');
$query = "update tb_atributo
	      set c_nombre_atributo = '".$_POST['nomatributo']."',
	      fecha_actualizacion  = '".$fecha."-".$hora."'
 		  where id_atributo = ".$_POST['idatributo'];	
try {
	$resultado= mysqli_query($conexion,$query);	
	$resp['error']=false;
} catch (Exception $e) {
	$resp['error']=true;	
}
//en PHP echo se usa para imprimir algo. json_encode imprime una respuesta json
echo json_encode($resp);
?>
