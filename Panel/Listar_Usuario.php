<?php

require_once "Controllers/conexion.php";

$page= $_GET['page'];

$query = "SELECT u.id_usuario,u.nombre,u.apellido,u.correo,u.contrasena,u.telefono,a.c_nombre_app,e.c_nombre_empresa,u.fecha_creacion,u.fecha_actualizacion,CASE WHEN u.id_sucursal=0 THEN 'Corporativo' ELSE 'Sucursal' END AS tipo FROM tb_usuario u
INNER JOIN tb_app a ON a.id_app=u.id_app
INNER JOIN tb_empresa e ON e.id_empresa=u.id_empresa
 		 order by u.id_usuario	
		  ";
$data = array();
try {
	$resultado = mysqli_query($conexion,$query);
	while( $row = mysqli_fetch_assoc($resultado)){
	    $data[$row['id_usuario']] = $row;
	}
	$resp['error']=false;	
} catch (Exception $e) {	
	$resp['error']=true;	
}

$resp['data']=$data;
echo json_encode($resp);

?>
