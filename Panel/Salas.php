<?php include 'Encabezado.php' ?>
  <div class="content-wrapper">
    <div class="col-sm-12" style="background: #ecf0f5;">
      <section id="main-content">
      <section class="wrapper" style="background: none;">
        <br>
         <div id="row-sala" class="row">
          <div class="col-md-12">
            <section style="border: 1px solid #e0e0e2;" class="panel">
              <header class="panel  panel-info">
                <div class="panel-heading">.: Gestion Salas :.
                  <span class="tools pull-right">
                  <a class="fa fa-chevron-down" href="javascript:;"></a>
                  </span>
                </div>
              </header>
              <div class="panel-body">
                <form class="" role="form" onsubmit="return false;">
                  <fieldset>
                    <div class="row">
                         <div class="col-sm-4">
                            <div class="form-group">
                              <button type="button" class="btn btn-success" id="btn-ver-sala"><i class="fa fa-bars"></i> Ver Salas<i class="preloader preloader-success hidden"></i></button>
                              <button type="button" class="btn btn-primary" id="btn-crear-sala"><i class="fa fa-plus-circle"></i> Nueva Sala <i class="preloader preloader-info hidden"></i></button>
                            </div>
                         </div>
                    </div>
                  </fieldset>
                </form>
                <div class="row" id="row-salas" style="display: none;">
                  <div class="col-md-12">
                    <section style="border: 1px solid #e0e0e2;" class="panel">
                      <table class="table table-striped table-hover" id="tb-sala">
                        <thead>
                          <tr>
                            <th>Sala</th>
                            <th>Creacion</th>
                            <th>Actualizacion</th>
                            <th>Editar</th>
                            <th>Eliminar</th>
                          </tr>
                        </thead>
                        <tbody>

                        </tbody>
                      </table>
                    </section>
                  </div>
                </div>
                <div class="row" id="pagination">
                </div>
              </div>
            </section>
          </div>
        </div>
    


      </section>
    </section>
    </div>
  </div>
</div>

<div class="modal fade" id="crear-sala" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-crear-sala">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Nueva Sala</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
              <div class="col-sm-12">
                  <label for="fono1">Sala</label>
                <input class="form-control nomsala" name="nomsala" type="text" required >
                <br>
  
              </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-crear-sala">Agregar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>

<div class="modal fade" id="editar-sala" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hide="true">
  <div class="modal-dialog">
    <div class="modal-content">
        <form class="form-horizontal tasi-form" method="POST" onsubmit="return false;" id="f-editar-sala">
          <input type="hidden" class="idsala">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-hide="true">&times;</button>
            <h4 class="modal-title">Actualizar Sala</h4>
          </div>
          <div class="modal-body">
            <div class="panel-body">
              <div class="form-group">
                <div class="col-sm-12">
                    <label for="fono1">Sala</label>
                  <input class="form-control nomsala" name="nomsala" type="text" required >
                                  
                </div>
              </div>
            </div>
          </div>
          <div class="modal-footer">
            <button class="btn btn-success" type="button" id="sbmt-editar-sala">Actualizar <i class="preloader preloader-success hidden"></i></button>
            <button data-dismiss="modal" class="btn btn-danger" type="button">Cancelar</button>
          </div>
        </form>
    </div>
  </div>
</div>





<div class="modal" id="cargando" data-backdrop="static" style="top:40%">
  <div class="modal-dialog" style="width: 155px;">
    <div class="modal-content">
      <div class="modal-body">
        <center><img src="loader.gif"></center>
      </div>
    </div>
  </div>
</div>

<script src="https://code.jquery.com/jquery-3.3.1.js" integrity="sha256-2Kok7MbOyxpgUVvAk/HJ2jigOSYS2auK4Pfzbm7uH60=" crossorigin="anonymous"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="//www.fuelcdn.com/fuelux/3.13.0/js/fuelux.min.js"></script>
<script src="Views/bower_components/fuelux/spinner.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
<!-- DESDE AQUI COMIENZA EL CODIGO JS QUE SE EJECUTA CUANDO LE DAS CLICKS A LOS BOTONES. ES JQUERY BASICO -->
<script>
  $(function(){
      $('#row-salas').hide();
   


    ////////////////////////////////////////////////////////
      $('#btn-ver-sala').click(function(){
         listarSalas(1);
      });
    
      /////////////////////////////////////////////////////////
      function listarSalas(page){
        $('#cargando').modal();
        var data={};
        data['page']=page;
        //SE OBTIENEN LAS ENCUESTAS HACIENDO LLAMADA AL ARCHIVO LISTAR_ENCUESTA.PHP. LA RESPUESTA ES LA VARIABLE RESP
        $.get('Listar_Sala.php',data,function(resp){
          $('#tb-sala tbody').empty();
          $('#pagination').empty();
          var i=0;
          $.each(resp.data,function(key,Sala){
            var tr = '';
            i++;
            tr += '<tr id="sala-'+ Sala.id_sala +'">';
            tr += '<td >' + Sala.sala + '</td>';
            tr += '<td >' + Sala.fecha_creacion + '</td>';
            tr += '<td >' + Sala.fecha_actualizacion + '</td>';
          
            tr += '<td>';
                      tr += '<center><button class="btn btn-xs btn-warning btn-editar-sala ttip" data-idsala="' + Sala.id_sala + '" data-placement="top" data-toggle="tooltip" title="Editar Sala"><i class="fa fa-pencil"></i> <i class="preloader preloader-info hide"></i></button></center>';
            tr += '</td>';
            if(Sala.c_tipo_sala=='D'){
              tr += '<td>';
                        tr += '<button class="btn btn-xs btn-primary btn-gestionar-sala ttip" data-idsala="' + Sala.id_sala + '" data-nomsala="'+Sala.sala+'" data-placement="top" data-toggle="tooltip" title=""><i class="fa fa-user fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
              tr += '</td>';

            }
            tr += '<td>';
                      tr += '<button class="btn btn-xs btn-danger btn-eliminar-sala ttip" data-idsala="' + Sala.id_sala + '" data-placement="top" data-toggle="tooltip" title="Borrar Sala"><i class="fa fa-trash-o fa-fw"></i> <i class="preloader preloader-info hide"></i></button>';
            tr += '</td>';
            tr += '</tr>';
            $('#tb-sala tbody').append(tr);
          });
          $('#tb-sala tbody .ttip').tooltip();
          $('#pagination').append(resp.links);
          $('#cargando').modal('toggle');
          $('#row-salas').show();
        },'json').fail(function(){
          $('#cargando').modal('toggle');
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });

      };
      ////////////////////////////////////////////////////////
      $('#btn-crear-sala').click(function(){
         $('#crear-sala').modal();
      });
      ///////////////////////////////////////////////////////
      $('#sbmt-crear-sala').click(function(){
        if( $('#crear-sala .nomsala').val() == ''){
          toastr.error('DEBE INGRESAR EL NOMBRE DE LA SALA');
          return false;
        }
    
        var data = {};
        data['nomsala'] = $('#crear-sala .nomsala').val();
           
        $('#crear-sala').modal('toggle');
        $('#cargando').modal();
        $.post('Agregar_Sala.php',data,function(resp){
          if(resp.error){
            $('#cargando').modal('toggle');
            toastr.error('ERROR: ' + resp.message);
            $('#crear-sala').modal('');
            return false;
          }else{
            $('#cargando').modal('toggle');
            $('#crear-sala .nomsala').val('');
            toastr.success('SALA AGREGADA CORRECTAMENTE');
            listarSalas(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       /////////////////////////////////////////////////////////////////////

      $(document).on('click','.btn-editar-sala',function(){
        var id = $(this).data('idsala');
        var data = {};
        data['id'] = id;
        $.post('Obtener_Sala.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#f-editar-sala .idsala').val(resp.data.id_sala);
            $('#f-editar-sala .nomsala').val(resp.data.sala);
            $('#f-editar-sala #tiposala').val(resp.data.c_tipo_sala);
            $('#editar-sala').modal();
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
      //////////////////////////////////////////////////////////////////////////////
      $('#sbmt-editar-sala').click(function(){
        if( $('#f-editar-sala .nomsala').val() == '0'){
          toastr.error('DEBE INGRESAR NOMBRE DE SALA');
          return false;
        }
        if( $('#f-editar-sala #tiposala').val() == '0'){
          toastr.error('DEBE INGRESAR UN TIPO');
          return false;
        }
        var data = {};
        data['idsala'] = $('#f-editar-sala .idsala').val();
        data['nomsala'] = $('#f-editar-sala .nomsala').val();
       
        $.post('Actualizar_Sala.php',data,function(resp){
          if(resp.error){
            toastr.error('ERROR: ' + resp.message);
          }else{
            $('#editar-sala').modal('toggle');
            toastr.success('SALA ACTUALIZADA CORRECTAMENTE');
            listarSalas(1);
          }
        },'json').fail(function(){
          toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
        });
      });
       ///////////////////////////////////////////////////////////////////////////
      $(document).on('click','.btn-eliminar-sala',function(){
        var id = $(this).data('idsala');
        swal({
            title: "",
            text: "¿Esta seguro que desea eliminar la sala?",
            type: "warning",
            showCancelButton: true,
            cancelButtonText: "Cancelar",
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Si",
            closeOnConfirm: true
        }, function(isConfirm){
            if (isConfirm) {
              var data = {};
              data['id'] = id;
              $.post('Eliminar_Sala.php',data,function(resp){
                if(resp.error){
                  toastr.error('ERROR: ' + resp.message);
                }else{
                  toastr.success('SALA ELIMINADA CORRECTAMENTE');
                  listarSalas(1);
                }

              },'json').fail(function(){
                toastr.error('ERROR AL ENVIAR/RECIBIR DATOS');
              });

            }
        });

      });
     
      /////////////////////////////////////////////////////////

      //////////////////////////////////////////////////////////////////////////////
  });
</script>
</body>
</html>
